﻿using SKE.CoreLib.Core;

namespace SKE.Feed.Domain
{
    /// <summary>
    /// Модель ленты
    /// </summary>
    public class Feed : BaseEntityInt
    {
        public List<FeedEvent> FeedEvents { get; set; }
    }
}
