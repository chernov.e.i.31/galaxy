﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SKE.Feed.Domain
{
    public class QueryParams
    {
        public IEnumerable<Guid> eventsId { get; set; }
        public IEnumerable<string> categories { get; set; }

        public string? city { get; set; }
        public int page { get; set; } = 0;
        public int pageSize { get; set; } = 20;
    }
}
