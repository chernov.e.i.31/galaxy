﻿using System.Collections.Specialized;

namespace SKE.Feed.Services.Implementations
{
    public class UriBuilderExt
    {
        private NameValueCollection _collection;
        private UriBuilder _builder;

        public UriBuilderExt(string uri)
        {
            _builder = new UriBuilder(uri);
            _collection = System.Web.HttpUtility.ParseQueryString(string.Empty);
        }

        public UriBuilderExt AddParameter(string key, string value)
        {
            _collection.Add(key, value);
            return this;
        }

        public Uri Uri
        {
            get
            {
                _builder.Query = _collection.ToString();
                return _builder.Uri;
            }
        }

    }
}
