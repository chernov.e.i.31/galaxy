﻿using AutoMapper;
using SKE.Core.DTO.EventManager;
using SKE.Core.gRPCLib.Shared;

namespace SKE.Feed.Services.Implementations.Mapping
{
    /// <summary>
    /// Профиль автомаппера для сущности города.
    /// </summary>
    public class PlaceDTOMappingsProfile : Profile
    {
        /// <summary>
        /// ctor
        /// </summary>
        public PlaceDTOMappingsProfile()
        {
            CreateMap<PlaceGRPC, PlaceDTO>()
                .ForMember(x => x.Coords, map => map.Ignore())
                .ForMember(x => x.ExternalId, map => map.Ignore())
                .ForMember(x => x.Categories, map => map.Ignore())
                .ForMember(x => x.Tags, map => map.Ignore())
                ;
        }
    }
}
