﻿using AutoMapper;
using SKE.Core.DTO.EventManager;
using SKE.Core.gRPCLib.Shared;

namespace SKE.Feed.Services.Implementations.Mapping
{
    /// <summary>
    /// Профиль автомаппера для сущности города.
    /// </summary>
    public class CityDTOMappingsProfile : Profile
    {
        /// <summary>
        /// ctor
        /// </summary>
        public CityDTOMappingsProfile()
        {
            CreateMap<LocationGRPC, CityDTO>()
                .ForMember(x => x.Coords, map => map.MapFrom(s => s.Coordinates));
        }
    }
}
