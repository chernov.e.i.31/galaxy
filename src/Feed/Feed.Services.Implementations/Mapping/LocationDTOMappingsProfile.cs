﻿using AutoMapper;
using SKE.Core.DTO.EventManager;
using SKE.Core.gRPCLib.Shared;

namespace SKE.Feed.Services.Implementations.Mapping
{
    /// <summary>
    /// Профиль автомаппера для сущности категорий событий.
    /// </summary>
    public class LocationDTOMappingsProfile : Profile
    {
        /// <summary>
        /// ctor
        /// </summary>
        public LocationDTOMappingsProfile()
        {
            CreateMap<LocationGRPC, LocationDTO>();
        }
    }
}
