﻿using AutoMapper;
using Microsoft.Extensions.Configuration;
using Newtonsoft.Json;
using SKE.Core.DTO.EventManager;
using SKE.Feed.Domain;
using SKE.Feed.Services.Abstractions;
using SKE.Feed.Services.Contracts;

namespace SKE.Feed.Services.Implementations
{
    /// <summary>
    /// Cервис работы с лентами
    /// </summary>
    public class FeedService : IFeedService
    {
        private readonly IFeedRepository _feedRepository;
        private readonly IMapper _mapper;
        private readonly IUserInterestsService _userInterestsService;
        private readonly IConfiguration _configuration;

        public FeedService(IFeedRepository feedRepository, IMapper mapper, IUserInterestsService userInterestsService, IConfiguration configuration)
        {
            _feedRepository = feedRepository;
            _mapper = mapper;
            _userInterestsService = userInterestsService;
            _configuration = configuration;
        }

        /// <summary>
        /// Получить ленту
        /// </summary>
        /// <param name="userId">Идентификатор пользователя</param>
        /// <param name="page">Номер страницы</param>
        /// <param name="pageSize">Кол-во элементов на странице</param>
        /// <returns>DTO ленты</returns>
        public async Task<FeedDTO> GetByUserIdAsync(int userId, IEnumerable<string> userInterests, int page, int pageSize)
        {
            // TODO Пока что получаем события напрямую из сервиса EventManager
            // В будущем надо перейти на получение событий из очереди

            //IntegrationResponseDTO<EventDTO> events = null;
            var eventManagerApiParam = IsDebug ? "EventManagerApiDebug" : "EventManagerApi";
            string skeeventmanagerApi = $"{_configuration[eventManagerApiParam]}Event";
            List<EventDTO>? events;
            var uriBuilder = new UriBuilderExt(skeeventmanagerApi);
            uriBuilder.AddParameter("IncludeDates", "true")
                      .AddParameter("IncludePlace", "true")
                      .AddParameter("IncludeImages", "true")
                      .AddParameter("IncludeLocation", "true")
                      .AddParameter("PageSize", pageSize.ToString())
                      .AddParameter("Page", page.ToString());
            foreach(var category in _userInterestsService.GetUserInterests(userId))
                uriBuilder.AddParameter("Categories", category);

            var handler = new HttpClientHandler() 
            { 
                ServerCertificateCustomValidationCallback = HttpClientHandler.DangerousAcceptAnyServerCertificateValidator
            };

            using (var httpClient = new HttpClient(handler))
            {
                using (var response = await httpClient.GetAsync(uriBuilder.Uri))
                {
                    string apiResponse = await response.Content.ReadAsStringAsync();
                    var integrationResponseDTO = JsonConvert.DeserializeObject<IntegrationResponseDTO<EventDTO>>(apiResponse);
                    events = integrationResponseDTO?.Data;
                }
            }

            var feed = new Domain.Feed()
            {
                FeedEvents = events != null ? _mapper.Map<List<EventDTO>, List<FeedEvent>>(events) : null
            };

            return _mapper.Map<Domain.Feed, FeedDTO>(feed);
        }

        /// <summary>
        /// Создать ленту
        /// </summary>
        /// <param name="feedDTO">DTO ленты</param>
        /// <returns>Идентификатор нового ленты</returns>
        public Task<int> Create(FeedDTO feedDTO)
        {
            throw new NotImplementedException();
        }

        /// <summary>
        /// Получить ленту
        /// </summary>
        /// <param name="userId">Идентификатор пользователя</param>
        /// <param name="page">Номер страницы</param>
        /// <param name="pageSize">Кол-во элементов на странице</param>
        /// <returns>DTO ленты</returns>
        public async Task<FeedDTO> GetAsync(QueryParams queryParams)
        {
            // TODO Пока что получаем события напрямую из сервиса EventManager
            // В будущем надо перейти на получение событий из очереди

            //IntegrationResponseDTO<EventDTO> events = null;
            var eventManagerApiParam = IsDebug ? "EventManagerApiDebug" : "EventManagerApi";
            string skeeventmanagerApi = $"{_configuration[eventManagerApiParam]}Event";
            List<EventDTO>? events;
            var uriBuilder = new UriBuilderExt(skeeventmanagerApi);
            uriBuilder.AddParameter("IncludeDates", "true")
                      .AddParameter("IncludePlace", "true")
                      .AddParameter("IncludeImages", "true")
                      .AddParameter("IncludeLocation", "true")
                      .AddParameter("PageSize", queryParams.pageSize.ToString())
                      .AddParameter("Page", queryParams.page.ToString());

            if (queryParams.eventsId.Count() != 0 && queryParams.eventsId != null)
            {
                uriBuilder.AddParameter("EventsId", queryParams.eventsId.ToString());
            }
            if (queryParams.categories.Count() != 0 && queryParams.categories != null)
            {
                uriBuilder.AddParameter("Categories", queryParams.categories.ToString());
            }
            if (!String.IsNullOrEmpty(queryParams.city))
            {
                uriBuilder.AddParameter("CityId", queryParams.city);
            }

            var handler = new HttpClientHandler()
            {
                ServerCertificateCustomValidationCallback = HttpClientHandler.DangerousAcceptAnyServerCertificateValidator
            };

            using (var httpClient = new HttpClient(handler))
            {
                using (var response = await httpClient.GetAsync(uriBuilder.Uri))
                {
                    string apiResponse = await response.Content.ReadAsStringAsync();
                    var integrationResponseDTO = JsonConvert.DeserializeObject<IntegrationResponseDTO<EventDTO>>(apiResponse);
                    events = integrationResponseDTO?.Data;
                }
            }

            var feed = new Domain.Feed()
            {
                FeedEvents = events != null ? _mapper.Map<List<EventDTO>, List<FeedEvent>>(events) : null
            };

            return _mapper.Map<Domain.Feed, FeedDTO>(feed);
        }

        public static bool IsDebug
        {
            get
            {
                bool isDebug = false;
                #if DEBUG
                       isDebug = true;
                #endif
                return isDebug;
            }
        }
    }
}