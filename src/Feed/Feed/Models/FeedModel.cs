﻿namespace SKE.Feed.Models
{
    /// <summary>
    /// Лента
    /// </summary>
    public class FeedModel
    {
        /// <summary>
        /// События ленты
        /// </summary>
        public List<FeedEventModel> Events { get; } = new();
    }
}
