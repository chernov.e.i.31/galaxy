﻿using System;
using AutoMapper;
using Microsoft.AspNetCore.Authentication.JwtBearer;
using Microsoft.Extensions.Options;
using Microsoft.IdentityModel.Tokens;
using SKE.Feed.DataBaseContext;
using SKE.Feed.Mapping;
using SKE.Feed.Repositories.Implementations;
using SKE.Feed.Services.Abstractions;
using SKE.Feed.Services.Implementations;
using SKE.Feed.Settings;
using System.Text;

namespace SKE.Feed
{
    public static class Registrator
    {
        public static IServiceCollection AddServices(this IServiceCollection services, IConfiguration configuration)
        {
            var applicationSettings = configuration.Get<ApplicationSettings>();
            services.AddSingleton(applicationSettings);
            return services
                .AddSingleton((IConfigurationRoot)configuration)
                .InstallServices()
                .ConfigureContext(applicationSettings.ConnectionString)
                .InstallRepositories();
        }

        public static IServiceCollection AddAutomapper(this IServiceCollection services)
        {
            return services.AddSingleton<IMapper>(new Mapper(GetMapperConfiguration()));
        }
  
        private static IServiceCollection InstallRepositories(this IServiceCollection serviceCollection)
        {
            return serviceCollection
                .AddTransient<IFeedRepository, FeedRepository>();
        }

        private static IServiceCollection InstallServices(this IServiceCollection serviceCollection)
        {
            serviceCollection
                .AddSingleton<IUserInterestsService, UserInterestsServiceStub>()
                .AddTransient<IFeedService, FeedServiceGRPC>();

            return serviceCollection;
        }

        private static MapperConfiguration GetMapperConfiguration()
        {
            var configuration = new MapperConfiguration(cfg =>
            {
                cfg.AddProfile<Services.Implementations.Mapping.DatesDTOMappingsProfile>();
                cfg.AddProfile<Services.Implementations.Mapping.SourceDTOMappingsProfile>();
                cfg.AddProfile<Services.Implementations.Mapping.ImageDTOMappingsProfile>();
                cfg.AddProfile<Services.Implementations.Mapping.CategoryEventDTOMappingsProfile>();
                cfg.AddProfile<Services.Implementations.Mapping.CoordinatesDTOMappingsProfile>();
                cfg.AddProfile<Services.Implementations.Mapping.CityDTOMappingsProfile>();
                cfg.AddProfile<Services.Implementations.Mapping.PlaceDTOMappingsProfile>();
                cfg.AddProfile<Services.Implementations.Mapping.LocationDTOMappingsProfile>();
                cfg.AddProfile<FeedMappingsProfile>();
                cfg.AddProfile<Services.Implementations.Mapping.FeedMappingsProfile>();
            });
            configuration.AssertConfigurationIsValid();
            return configuration;
        }
    }
}
