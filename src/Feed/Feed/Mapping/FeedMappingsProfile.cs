﻿using AutoMapper;
using SKE.Feed.Models;
using SKE.Feed.Services.Contracts;

namespace SKE.Feed.Mapping
{
    /// <summary>
    /// Профиль автомаппера для сущности ленты
    /// </summary>
    public class FeedMappingsProfile : Profile
    {
        public FeedMappingsProfile()
        {
            CreateMap<FeedEventModel, FeedEventDTO>();
            CreateMap<FeedEventDTO, FeedEventModel>();

            CreateMap<FeedModel, FeedDTO>();
            CreateMap<FeedDTO, FeedModel>();
        }
    }
}
