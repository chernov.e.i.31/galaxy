﻿using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Logging;

namespace SKE.Feed.DataBaseContext
{
    public class DataContext : DbContext
    {
        public DbSet<Domain.Feed> Feeds { get; set; }

        public DataContext()
        {
        }

        public DataContext(DbContextOptions<DataContext> options)
            : base(options)
        {
        }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            base.OnModelCreating(modelBuilder);
        }

        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
            optionsBuilder.LogTo(Console.WriteLine, LogLevel.Information);
        }
    }
}
