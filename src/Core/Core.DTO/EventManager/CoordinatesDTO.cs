﻿using System.ComponentModel.DataAnnotations.Schema;

namespace SKE.Core.DTO.EventManager
{
    public class CoordinatesDTO
    {
        /// <summary>
        /// идентификатор
        /// </summary>
        public Guid Id { get; set; }
        /// <summary>
        /// широта
        /// </summary>
        [Column(TypeName = "decimal(19, 15)")]
        public decimal? Lat { get; set; }
        /// <summary>
        /// высота
        /// </summary>
        [Column(TypeName = "decimal(19, 15)")]
        public decimal? Lon { get; set; }
    }
}
