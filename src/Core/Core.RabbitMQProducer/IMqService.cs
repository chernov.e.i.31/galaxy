﻿namespace SKE.Core.RabbitMQProducer
{
    public interface IMqService<T>
    {
        void SendMessage(T obj, string quequeName);
    }
}
