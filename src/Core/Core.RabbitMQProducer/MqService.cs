﻿using Microsoft.Extensions.Configuration;
using SKE.Core.RabbitMQProducer;

namespace Core.RabbitMQProducer
{
    public class MqService : IMqService<object>
    {
        private readonly RabbitMqService _rabbitMqQueueService;

        public MqService(IConfiguration configuration)
        {
            _rabbitMqQueueService = new RabbitMqService(configuration);
        }

        public void SendMessage(object obj, string quequeName)
        {
            _rabbitMqQueueService.SendMessage(obj, quequeName);
        }
    }
}