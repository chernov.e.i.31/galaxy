import * as React from 'react';
import Box from '@mui/material/Box';
import TextField from '@mui/material/TextField';
import Stack from '@mui/material/Stack';
import Button from '@mui/material/Button';
import MenuItem from '@mui/material/MenuItem';
import Select from '@mui/material/Select';
import InputLabel from '@mui/material/InputLabel';
import { Typography } from '@mui/material';
import { alignProperty } from '@mui/material/styles/cssUtils';
import dayjs, { Dayjs } from 'dayjs';
import { DatePicker } from '@mui/x-date-pickers/DatePicker';
import { DemoContainer } from '@mui/x-date-pickers/internals/demo';
import { AdapterDayjs } from '@mui/x-date-pickers/AdapterDayjs';
import { LocalizationProvider } from '@mui/x-date-pickers/LocalizationProvider';
import { useState } from 'react';
import Chip from '@mui/material/Chip';
import Autocomplete from '@mui/material/Autocomplete';
import { useEffect } from 'react';
import Constants from '../Constants';
import UseToken from '../UseToken';
import UserService from '../Services/UserService';
import IdentityService from '../Services/IdentityService';
import getPreferences from '../Services/Preferences';
import getLocations from '../Services/Location';

export default function Profile() {
 
  const [email, setEmail] = useState(UserService.getEmail());
  const [firstName, setFirstName] = useState();
  const [lastName, setLastName] = useState();
  const [dateOfBirdthUser, setDateOfBirdthUser] = useState(dayjs(new Date()));
  const [password, setPassword] = useState();
  const [passwordConfirmUser, setPasswordConfirm] = useState();
  const [preferences, setPreferences] = useState([]);
  const [preferencesData, setPreferencesData] = useState([]);

  const [loc, setLocation] = useState([]);
  const [locationPlatform, setLocationPlatform] = useState([]);

  const [emailError, setEmailError] = useState(false);
  const [firstNameError, setFirstNameError] = useState(false);
  const [lastNameError, setLastNameError] = useState(false);
  const [dateOfBirdthUserError, setDateOfBirdthUserError] = useState(false); 
  const [passwordConfirmUserError, setPasswordConfirmUserError] = useState(false);
  const [preferencesError, setPreferencesError] = useState(false);
  const [locationError, setLocationError] = useState(false);

  const [emailErrorText, setEmailErrorText] = useState();
  const [firstNameErrorText, setFirstNameErrorText] = useState();
  const [lastNameErrorText, setLastNameErrorText] = useState();
  const [dateOfBirdthUserErrorText, setDateOfBirdthUserErrorText] = useState(); 
  const [passwordConfirmUserErrorText, setPasswordConfirmUserErrorText] = useState();
  const [preferencesErrorText, setPreferencesErrorText] = useState();
  const [locationErrorText, setLocationErrorText] = useState();

  const [loaded, setLoaded] = useState(0);
  const [value, setValue] = React.useState(dayjs(new Date()));
  
  const handleSubmit = async e => {
    e.preventDefault();
    const gender = 'gender';
    const username = email;
    const DateOfBirth = dateOfBirdthUser.format('DD.MM.YYYY');
    const location = loc.slug;

    var regData = {
      username,
      password,
      email,
      firstName,
      lastName,
      gender,
      DateOfBirth,
      preferences,
      location
    };
    //TODO ошибка
    var result = await IdentityService.update(regData);
    if(result.status === `ok`)
    {
      window.location.href = 'registred';
    }
    else
    {
      setDateOfBirdthUserError(false);
      setEmailError(false);
      setPreferencesError(false);
      setLocationError(false);
      setLastNameError(false);
      setFirstNameError(false);
      setDateOfBirdthUserErrorText('');
      setEmailErrorText('');
      setPreferencesErrorText('');
      setLocationErrorText('');
      setLastNameErrorText('');
      setFirstNameErrorText('');

      var errors = result.error;
      if(errors.errors !== undefined)
      {
        if(errors.errors.DateOfBirth !== undefined)
        {
          setDateOfBirdthUserError(true);
          setDateOfBirdthUserErrorText(errors.errors.DateOfBirth[0]);
        }
        if(errors.errors.Email !== undefined)
        {
          setEmailError(true);
          setEmailErrorText(errors.errors.Email[0]);
        }
        if(errors.errors.Preferences !== undefined)
        {
          setPreferencesError(true);
          setPreferencesErrorText(errors.errors.Preferences[0]);
        }
        if(errors.errors.LastName !== undefined)
        {
          setLastNameError(true);
          setLastNameErrorText(errors.errors.LastName[0]);
        }
        if(errors.errors.FirstName !== undefined)
        {
          setFirstNameError(true);
          setFirstNameErrorText(errors.errors.FirstName[0]);
        }
        if(errors.errors.propertyName !== undefined)
          {
            setLocationError(true);
            setLocationErrorText(errors.errors.errorMessage);
          }
      }
      else
      {
        setDateOfBirdthUserError(false);
        setEmailError(false);
        setPreferencesError(false);
        setLocationError(false);
        setLastNameError(false);
        setFirstNameError(false);
      }
    }
  }

  useEffect(() => {
    if(loaded > 0)
    {
      return;
    }
    const f = async()=>{
      const tokenss = UseToken().token;
      const decodedToken = UserService.decodeJWT(tokenss);

      var datas = await getPreferences();
      setPreferencesData(datas);
      var datal = await getLocations();
      setLocationPlatform(datal);
      var datau = await IdentityService.userInfo(decodedToken.email);
      setLastName(datau.lastName);
      setFirstName(datau.firstName);
      setValue(dayjs(datau.dateOfBirth));
      datau.pref.forEach((pr, index) => {
        preferences.push(pr.preference)
      });      
      setLocation(datal.find((obj) => {return datau.location === obj.slug}));      
      setLoaded(1);
    }
    f();
    
  },[preferences,firstName, lastName, dateOfBirdthUser,loc]);
  
  return (
    <Box>
      <Stack component="form" sx={{'& .MuiTextField-root': { m: 1, width: '50ch' },
          '& .MuiButton-root': { m: 1, width: '28ch' }, p:3}} noValidate autoComplete="off" 
          direction="column" justifyContent="center" alignItems="center">
        <Typography sx={{p:3}}> Настройки профиля: </Typography>
        <TextField helperText={firstNameErrorText} error={firstNameError} id="outlined-required"
            value={firstName} onChange={e => setFirstName(e.target.value)} />
        <TextField helperText={lastNameErrorText} error={lastNameError} id="outlined-required"
            value={lastName}  onChange={e => setLastName(e.target.value)}/>
        <Box  sx={{ display: 'flex', '& > :not(style)': { m: 1,},}}>
          <LocalizationProvider dateAdapter={AdapterDayjs}>
            <DatePicker labelId="demo-simple-select-filled-label1" value={value} label="Дата вашего рождения" 
              error={dateOfBirdthUserError} slotProps={{ textField: { helperText: dateOfBirdthUserErrorText,
              error:dateOfBirdthUserError},}} onChange={e => setDateOfBirdthUser(e)}/>
          </LocalizationProvider>
        </Box>
        <Autocomplete required multiple id="size-small-standard-multi" size="small"
          value={preferencesData.filter((obj) => {return preferences.includes(obj.slug)})}
          options={preferencesData} 
          getOptionLabel={(option) => option.name} 
          getOptionSelected={(option, value) => option.slug === value.slug }
          error={preferencesError}
          onChange={(value, item) => { setPreferences(item.map((i) => i.slug))}}
          renderInput={(params) => (
            <TextField
              {...params}
              variant="standard"
              label="Выберите ваши интересы"
              placeholder="Интересы"
              helperText={preferencesErrorText}
              error={preferencesError}
            />
          )}/>
        <Autocomplete required
          id="size-small-standard-location"
          size="small"
          value={loc}
          options={locationPlatform}
          getOptionLabel={(option) => option.name}
          getOptionSelected={(option, value) => option.slug === value.slug }
          error={locationError}
          onChange={(value, item) => {setLocation(item)}}
          renderInput={(params) => (
            <TextField
              {...params}
              variant="standard"
              label="Выберите Город"
              placeholder="город"
              helperText={locationErrorText}
              error={locationError}
            />
          )}
        />
        <Box>
          <Button variant="contained" color="primary" type="submit" onClick={handleSubmit}>Отправить</Button>
          <Button variant="contained" color="primary" type="submit">Отмена</Button>
        </Box>
      </Stack>
    </Box>
  );
}