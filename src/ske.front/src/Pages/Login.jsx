import * as React from 'react';
import Box from '@mui/material/Box';
import TextField from '@mui/material/TextField';
import Stack from '@mui/material/Stack';
import Button from '@mui/material/Button';
import { alignProperty } from '@mui/material/styles/cssUtils';
import { Typography } from '@mui/material';
import PropTypes from 'prop-types';
import { useState } from 'react';
import { Form } from 'react-router-dom';
import Constants from '../Constants';
import UserService from '../Services/UserService';
import { useEffect } from 'react';
import IdentityService from '../Services/IdentityService';

export default function Login({ setToken }) {
  const [username, setUserName] = useState();
  const [password, setPassword] = useState();
  const [autentificated, setAuth] = useState();

  if(setToken === undefined)
  {
    setToken = (userToken)=>{
        UserService.setSessionVars(JSON.stringify(userToken));
    }
  }

  /*useEffect(() => {
    setUserName('test@example.com');
    setPassword('Password123');
  },[username,password]);*/

  const handleSubmit = async e => {
    e.preventDefault();
    const token = await IdentityService.login({username,password});
    if(token.status === undefined)
    {
      setToken(token);
      setAuth(true);
      window.location.href = '/registred';
    }
    else
    {
      setAuth(false);
    }
  }

  const ShowError = (auth) => {
    if (auth === false) {
      return <Typography sx={{color:'red'}}> Неверное имя пользователя или пароль</Typography>;
    }
  }

  return (
    <Box>
      <Typography sx={{p:3}}>
        Введите данные для входа:
      </Typography>
      <Stack component="form"
        sx={{'& .MuiTextField-root': { m: 1, width: '50ch' },
        '& .MuiButton-root': { m: 1, width: '28ch' }, p:3}}
        noValidate
        autoComplete="off" 
        direction="column"
        justifyContent="center"
        alignItems="center">
        <TextField
          required
          id="outlined-required"
          label="Электронная почта"
          onChange={e => setUserName(e.target.value)}
        />
        <TextField
          id="filled-password-input"
          label="Пароль"
          type="password"
          autoComplete="current-password"
          variant="filled"
          onChange={e => setPassword(e.target.value)}
        /> 
        {ShowError(autentificated)}
        <Box>
            <Button variant="contained" color="primary" type="submit" onClick={handleSubmit}>Отправить</Button>
            <Button variant="contained" color="primary" type="submit">Отмена</Button>
        </Box>
      </Stack>
    </Box>
  );
}
//TODO здесь ошибка
Login.propTypes = {
  setToken: PropTypes.func.isRequired
}