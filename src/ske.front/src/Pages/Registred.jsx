import { styled } from '@mui/material/styles';
import Box from '@mui/material/Box';
import Paper from '@mui/material/Paper';
import Grid from '@mui/material/Grid';
import EventItem from '../Components/EventItem'
import Tags from '../Components/Tags'
import FeedService from '../Services/FeedService';
import SkeTreeView from '../Components/SkeTreeView';
import Login from '../Pages/Login'
import UseToken from '../UseToken';
import { useState, useEffect } from 'react';
import UserService from '../Services/UserService';
import Button from '@mui/material/Button';
import IdentityService from '../Services/IdentityService';

const Item = styled(Paper)(({ theme }) => ({
  backgroundColor: theme.palette.mode === 'dark' ? '#1A2027' : '#fff',
  ...theme.typography.body2,
  padding: theme.spacing(1),
  textAlign: 'center',
  color: theme.palette.text.secondary,
}));

export default function Registred(props) {
  const [oldCircle, setOldCircle] = useState();
  const [selectCircle, setSelectCircle] = useState();
  const [sizeCat, setSizeCat ] = useState(0);
  const [selectLoc, selectL] = useState(props.selectedLocation);

  const [events, setEvents] = useState([]);
  const [isChild, setChild] = useState(false);

  const [eventsDatas, setEventsDatas] = useState([]);
  const [loaded, setLoaded] = useState(false);
  const [firstLoad, setFirstLoad] = useState(true);

  const [page, setPage] = useState(0);
  const [pageSize, setSize] = useState(20);
  const [disableNextButton, setNextButton] = useState(false);
  
  useEffect(() => {
    if(loaded)
    {
      return;
    }
    f();    
    setLoaded(true);
    setFirstLoad(false);
    datap(events, isChild);
  },[eventsDatas, loaded]);

  const f = async()=>{
    if(firstLoad){
      const tokenss = UseToken().token;
      const decodedToken = UserService.decodeJWT(tokenss);
      var datau = await IdentityService.userInfo(decodedToken.email);
      var defCat = [];
      datau.pref.forEach((pr, index) => {
        defCat.push(pr.preference)
      });
      props.setCat(defCat)
      props.selectLocation(datau.location)
    }
  }

  const datap = async ()=>{
    var d = []
    if(page === 0)
    {
      if (events.length !== 0 || isChild === false)
      {
        d = (await FeedService.getData(UserService.getUserId(), props.selectedLocation, page, pageSize, events, props.selectedCat)).events;
        setEventsDatas(d);
      }
      else
      {
        setEventsDatas([])
      }
      setNextButton(false);
    }
    else
    {
      d = Array.from(eventsDatas);
      var newData = (await FeedService.getData(UserService.getUserId(), props.selectedLocation, page, pageSize, events, props.selectedCat)).events;
      d = d.concat(newData);
      if(newData.length !== pageSize)
      {
        setNextButton(true);
      }
    }
    setEventsDatas(d);    
  };

  const checkState = () => {
    var stateChange = false;
    if(selectLoc !== props.selectedLocation)
    {
      selectL(props.selectedLocation);
      stateChange = true;
    }
    if(sizeCat !== props.selectedCat.length)
    {
      setSizeCat(props.selectedCat.length)
      stateChange = true;
    }
    if(oldCircle !== selectCircle)
    {
      setOldCircle(selectCircle)
      stateChange = true;
    }

    if(stateChange === true)
    {
      setPage(0);
      setLoaded(false);
    }
  }

  const setNextEvent = async () => {
    setPage(page+1);
    setLoaded(false);
  };
  
  checkState();

  if(UserService.IsAuthorized() === false)
  {
    return (<Login/>);
  }

  return (    
    <Box sx={{ p: 0}}>
      <Box sx={{ width:'100%', p: 2, bgcolor: 'background.default', display: 'grid',
          gridTemplateColumns: { md: '3fr 1fr ' }, gap: 1,}}>
        <Box>
          <Tags selectedCat={props.selectedCat} setCat={props.setCat}/>
          <Box sx={{ p: 2, bgcolor: 'background.default', display: 'grid',
            gridTemplateColumns: { md: '1fr 1fr 1fr ' }, gap: 1, }}>
            {eventsDatas.map((itemDatas, index) => (
              <EventItem key={index} itemData={itemDatas} sindex={itemDatas.id} selectedCircle = {selectCircle} />
            ))} 
          </Box>
          <Box display="flex" justifyContent="center" sx={{ p: 0 ,}}>
            <Button variant="contained" color="primary" onClick={setNextEvent }  disabled={disableNextButton}>
              Показать ещё
            </Button>
          </Box>
        </Box> 
        <Box sx={{p:1}}>
          <Paper sx={{ position: 'fixed', display:'block', zIndex:13234, width:'100%', top: '60', right:'100', height:'85%', }} >
            <SkeTreeView setEvents={setEvents} setChild={setChild} setSelectCircle = {setSelectCircle} />
          </Paper>
        </Box>
      </Box>
    </Box>
  );
}