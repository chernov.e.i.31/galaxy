import * as React from 'react';
import Box from '@mui/material/Box';
import { useTheme } from '@mui/material/styles';
import MobileStepper from '@mui/material/MobileStepper';
import Paper from '@mui/material/Paper';
import Stack from '@mui/material/Stack';
import TextField from '@mui/material/TextField';
import Typography from '@mui/material/Typography';
import Button from '@mui/material/Button';
import KeyboardArrowLeft from '@mui/icons-material/KeyboardArrowLeft';
import KeyboardArrowRight from '@mui/icons-material/KeyboardArrowRight';
import Constants from '../Constants';
import UseToken from '../UseToken';
import CircleService from '../Services/CircleService';
import UserService from '../Services/UserService';
import SkeList from '../Components/SkeList';
import {useState, useReducer} from 'react';

const steps = [
  {
    label: 'Создайте круг',
    description: `Укажите его название, например круг друзей, знакомых, родственников`,
    id:'',
    value:'',
    error:'',
    handleSubmit: async e => {
      var user = UserService.getEmail();
      var name = steps[0].id;
      var result = await CircleService.createCircle(user, name) 
      steps[0].value = result.data;
      steps[0].error = result.error;
    },
    renderer:function()
    {
      const setCircleName = async e => 
      {
        steps[0].id = e;
        steps[0].error = ``;
      };
      return(
        <Box>
          <Stack component="form" sx={{'& .MuiTextField-root': { m: 1, width: '50ch' }, '& .MuiButton-root': { m: 1, width: '28ch' }, p:3}}
              noValidate autoComplete="off" direction="column" justifyContent="center" alignItems="center">
            <Typography sx={{p:3}}>
                Введите название круга:
            </Typography>
            
            <TextField required id="outlined-required" label="Наименование" onChange={e => setCircleName(e.target.value)}/>
            <Typography sx={{p:3}}>
                {steps[0].error}
            </Typography>
          </Stack>
        </Box>
      );
    }
  },//   <Button variant="contained" color="primary" type="submit" onClick={this.handleSubmit}>Создать</Button>
  {
    cmp:function(){},
    label: 'Пригласите пользователей',
    description:
    'Добавляйте пользователей в круг используя email',
    id:'',
    getItems:function(){return steps[1].itemsArray;},
    itemsArray:[],
    handleSubmit: async e => {
      var userMail = steps[1].id;
      steps[1].itemsArray.push(userMail);
      await CircleService.addUserToCircle(steps[0].value, userMail) 
    },
    renderer:function()
    { 
      const setUserForAddName = async e => 
      {
        steps[1].id = e;
      }; 
      return(
        <Box>
          <Stack>
            <Box sx={{p:1}}> 
              <TextField sx={{width:360}} required id="outlined-required" label="email" onChange={e => setUserForAddName(e.target.value)}/>
            </Box>
            <Box sx={{p:1}}>
              <Button sx={{width:360}} variant="contained" color="primary" type="submit" onClick={this.handleSubmit}>Добавить в круг {steps[0].id.trim(5)}...</Button>
            </Box>
            <Box sx={{p:1}}>
              <SkeList items={this.getItems}/>
            </Box>
          </Stack>
        </Box>
      );
    }
  }
];

export default function CreateCircle() {
  const [ignored, forceUpdate] = useReducer(x => x + 1, 0);
  const theme = useTheme();
  const [activeStep, setActiveStep] = React.useState(0);
  const maxSteps = steps.length;
  steps[1].cmp = this;

  const  handleNext = async () => {
    await steps[activeStep].handleSubmit();
    if(steps[activeStep].error === ``) {
      setActiveStep((prevActiveStep) => prevActiveStep + 1);
    }
    else {

    }
  };

  const handleBack = () => {
    setActiveStep((prevActiveStep) => prevActiveStep - 1);
  };

  const handleRegistration = () =>{
    window.location.href = '/registred';
  }

  return (
    <Box sx={{ maxWidth: 400, flexGrow: 1 }}>
      <Paper square elevation={0} sx={{display: 'flex',alignItems: 'center',height: 50,pl: 2,bgcolor: 'background.default',}}>
        <Typography>{steps[activeStep].label}</Typography>
      </Paper>
      <Box sx={{ height: 400, maxWidth: 450, width: '100%', p: 2 }}>
        <Box sx={{p:0}}>
           {steps[activeStep].description}
        </Box>
        <Box sx={{p:1}}>
           {steps[activeStep].renderer()}
        </Box>
      </Box>
      <MobileStepper variant="text" steps={maxSteps} position="static" activeStep={activeStep} 
          nextButton={
            <Button size="small" onClick={activeStep !== maxSteps - 1 ? handleNext : handleRegistration }>
              { 
                (activeStep !== maxSteps - 1 ? "Далее" : "Готово")
              }
              { theme.direction === 'rtl'?(<KeyboardArrowLeft />) :(<KeyboardArrowRight />)
              }
            </Button>
          }
          backButton={
            <Button size="small" onClick={handleBack} disabled={activeStep === 0}>
              {theme.direction === 'rtl' ? (<KeyboardArrowRight />) : (<KeyboardArrowLeft />)}
              Назад
            </Button>
        }
      />
    </Box>
  );
}