import Constants from '../Constants';
import UseToken from '../UseToken';

export default {
  async getLikeByUserEvent(eventId, userId)
  {
    var token = UseToken().token;
    //var url = `${Constants.HOST}:60004/api/v1/Likes/getLikeByEventUser?eventId=${eventId}&userId=${userId}`;
    var url = `${Constants.GATEWAY_HOST}/ActivityService/Likes/getLikeByEventUser?eventId=${eventId}&userId=${userId}`;
    var datas = await fetch(url,{
      method: 'GET',
      headers: {
        'Content-Type': 'application/json',
        'Authorization': 'Bearer ' + token
      }
    })
    .then(data => data.json());
    return datas;
  },
  async addLike(eventId, userId)
  {
    var token = UseToken().token;
    //var datas = await fetch(`${Constants.HOST}:60004/api/v1/Likes/AddLike?eventId=${eventId}&userId=${userId}`,{
    var datas = await fetch(`${Constants.GATEWAY_HOST}/ActivityService/Likes/AddLike?eventId=${eventId}&userId=${userId}`,{
      method: 'POST',
      headers: {
        'Content-Type': 'application/json',
        'Authorization': 'Bearer ' + token,
      }
    })
    return datas;
  },
  async removeLike(eventId, userId)
  {
    var token = UseToken().token;
    //var datas = await fetch(`${Constants.HOST}:60004/api/v1/Likes/DeleteLike?eventId=${eventId}&userId=${userId}`,{
    var datas = await fetch(`${Constants.GATEWAY_HOST}/ActivityService/Likes/DeleteLike?eventId=${eventId}&userId=${userId}`,{
      method: 'DELETE',
      headers: {
        'Content-Type': 'application/json',
        'Authorization': 'Bearer ' + token,
      }
    })
    return datas;
  }
}