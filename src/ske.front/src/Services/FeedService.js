import mockData from '../back_mock.json';
import UseToken from '../UseToken';
import Constants from '../Constants';

export default {
  async  getData(userId, city, page, pageSize, eventsId, categories)
  {
    var token = UseToken().token;
    var eventString =``;
    eventsId.forEach(element => {
      eventString += `&eventsId=${element}`
    });
    var categoriesString =``;
    categories.forEach(element => {
      categoriesString += `&categories=${element}`
    });
    var url = `${Constants.GATEWAY_HOST}/FeedService/`
      /*+ (token === undefined ? ``: `${userId}`)*/
      + `?page=${page}`
      + `&pageSize=${pageSize}`
      +  (city !== undefined && city !== ``? `&city=${city}`:``)
      + eventString
      + categoriesString;

    var datas = await fetch(url,{
        method: 'GET',
        headers: {
          'Content-Type': 'application/json',
          'Authorization': 'Bearer ' + token
        }
      })
      .then(data => data.json());
      if(datas === undefined)
      {
        return {events:[] };
      }
      return datas;
  },
  async  getDataById(eventsId)
  {
    var url = `${Constants.GATEWAY_HOST}/FeedService/?eventsId=${eventsId}`;
    var datas = await fetch(url,{
        method: 'GET',
      })
      .then(data => data.json());
      if(datas === undefined)
      {
        return {events:[] };
      }
      return datas;
  }
}