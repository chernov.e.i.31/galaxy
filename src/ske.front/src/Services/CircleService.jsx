import Constants from '../Constants';
import UseToken from '../UseToken';
import UserService from '../Services/UserService';


export default {
    getUserCircles:async function()
    {
        if(UserService.IsAuthorized() === false)
        {
            return [{users:[]}];
        }
        var token = UseToken().token;
        var email = UserService.getEmail();
        var url = `${Constants.GATEWAY_HOST}/CircleService/Circle?user=${email}`;
        var datas = await fetch(url,{
            method: 'GET',
            headers: {
            'Content-Type': 'application/json',
            'Authorization': 'Bearer ' + token
            }
        })
        .then(data => data.json());
        if(datas === undefined)
        {
            return {events:[]};
        }
         
        return datas;
    },
    createCircle: async function(user, name)
    {
        var result = { data:``, error:``};
        if(UserService.IsAuthorized() === false)
        {
            return result;
        }
        var token = UseToken().token;
        var regData = 
        {
            user,
            name
        };
        var postBody = JSON.stringify(regData) 
        //var url = `${Constants.HOST}:60006/api/v1/Circle`,{
        var datas = await fetch(`${Constants.GATEWAY_HOST}/CircleService/Circle`,{
          method: 'POST',
          headers: {
            'Content-Type': 'application/json',
            'Authorization': 'Bearer ' + token
          },
          body:postBody
        })
        .then(async res => {          
          if(!res.ok) {
            result.error = await res.text();
          }
          else {
            return res.json();
          }    
        });
       
        if(datas === undefined)
        {
            return result;
        }
        result.data = datas
        return result;
    },
    addUserToCircle: async function(circleId, user)
    {
        var token = UseToken().token;
        //var url = `${Constants.HOST}:60006/api/v1/User?circleId=${circleId}&user=${user}`,{
        var datas = await fetch(`${Constants.GATEWAY_HOST}/CircleService/User?circleId=${circleId}&user=${user}`,{
            method: 'PUT',
            headers: {
              'Content-Type': 'application/json',
              'Authorization': 'Bearer ' + token
            },
          });
        if(!datas.ok)
        {
          console.log('error:'+datas);
        }
        else
        {
          console.log('All ok');
        }
    },
    addEventToCircle: async function(circleId, eventId)
    {
        var token = UseToken().token;
        //var datas = await fetch(`${Constants.HOST}:60006/api/v1/Event?circleId=${circleId}&eventId=${eventId}`,{
        var datas = await fetch(`${Constants.GATEWAY_HOST}/CircleService/Event?circleId=${circleId}&eventId=${eventId}`,{
            method: 'PUT',
            headers: {
              'Content-Type': 'application/json',
              'Authorization': 'Bearer ' + token
            },
          });
        if(!datas.ok)
        {
          console.log('error:'+datas);
        }
        else
        {
          console.log('All ok');
        }
    }
}