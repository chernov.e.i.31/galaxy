import * as React from 'react';
import { styled } from '@mui/material/styles';
import Card from '@mui/material/Card';
import CardHeader from '@mui/material/CardHeader';
import CardMedia from '@mui/material/CardMedia';
import CardContent from '@mui/material/CardContent';
import CardActions from '@mui/material/CardActions';
import Collapse from '@mui/material/Collapse';
import Avatar from '@mui/material/Avatar';
import IconButton, { IconButtonProps } from '@mui/material/IconButton';
import Typography from '@mui/material/Typography';
import { red } from '@mui/material/colors';
import FavoriteIcon from '@mui/icons-material/Favorite';
import ShareIcon from '@mui/icons-material/Share';
import ExpandMoreIcon from '@mui/icons-material/ExpandMore';
import MoreVertIcon from '@mui/icons-material/MoreVert';
import Link from '@mui/material/Link';
import {useNavigate} from 'react-router-dom';
import dayjs, { Dayjs } from 'dayjs';
import Like from './Like';

interface ExpandMoreProps extends IconButtonProps {
  expand: boolean;
}

const ExpandMore = styled((props: ExpandMoreProps) => {
    const { expand, ...other } = props;
    return <IconButton {...other} />;
  })
  (({ theme, expand }) => ({
    transform: !expand ? 'rotate(0deg)' : 'rotate(180deg)',
    marginLeft: 'auto',
    transition: theme.transitions.create('transform', {
      duration: theme.transitions.duration.shortest,
    }),
  }));
// посмотреть есть ли SSL HTTPS в запущеном докере
export default function EventItem(props) {
  const [expanded, setExpanded] = React.useState(false);
  const handleExpandClick = () => { setExpanded(!expanded); };
  
  var dateOf = dayjs(props.itemData.dates[props.itemData.dates.length - 1].start);
  var dateOfStr = dateOf.format('DD.MM.YYYY HH:mm');
  dateOfStr = dateOf.year() <= "2000" ? "" : dateOfStr;
  return (
    <Card sx={{  }}>
      <CardHeader sx={{cursor:'pointer'}} avatar={ <Avatar sx={{ bgcolor: red[500] }} aria-label="recipe"> R </Avatar> }
        title={props.itemData.shortTitle} subheader={dateOfStr} 
        onClick={() => { window.location.href = '/details/'+props.sindex + (props.selectedCircle !== undefined && props.selectedCircle !== ''? '?circle='+ props.selectedCircle : ''); }}
      />
      <CardMedia component="img" height="194" image={props.itemData.images[0].imageUrl} alt="Paella dish"/>
      <CardContent>
        <Typography variant="body2" color="text.secondary">
          <div dangerouslySetInnerHTML={{__html: props.itemData.description}} />
        </Typography>
      </CardContent>
      <CardActions disableSpacing>
        <Like itemDetails={props.itemData} />
        <IconButton aria-label="share">
          <ShareIcon />
        </IconButton>
        <ExpandMore expand={expanded} onClick={handleExpandClick} aria-expanded={expanded} aria-label="show more">
          <ExpandMoreIcon />
        </ExpandMore>
      </CardActions>
      <Collapse in={expanded} timeout="auto" unmountOnExit>
        <CardContent>
          <Typography paragraph> 
            <div dangerouslySetInnerHTML={{__html: props.itemData.bodyText}} />
          </Typography>
        </CardContent>
      </Collapse>
    </Card>
  );
}