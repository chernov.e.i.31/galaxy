import * as React from 'react';
import { useState, useEffect } from 'react';
import Box from '@mui/joy/Box';
import Button from '@mui/joy/Button';
import FormControl from '@mui/joy/FormControl';
import FormLabel from '@mui/joy/FormLabel';
import Textarea from '@mui/joy/Textarea';
import IconButton from '@mui/joy/IconButton';
import Menu from '@mui/joy/Menu';
import MenuItem from '@mui/joy/MenuItem';
import ListItemDecorator from '@mui/joy/ListItemDecorator';
import FormatBold from '@mui/icons-material/FormatBold';
import FormatItalic from '@mui/icons-material/FormatItalic';
import KeyboardArrowDown from '@mui/icons-material/KeyboardArrowDown';
import Check from '@mui/icons-material/Check';
import { alignProperty } from '@mui/material/styles/cssUtils';
import { Typography } from '@mui/material';
import Paper from '@mui/material/Paper';
import CommentService from '../Services/CommentService';
import UserService from '../Services/UserService';
import IdentityService from '../Services/IdentityService';
import Grid from '@mui/material/Unstable_Grid2';
import Card from '@mui/material/Card';
import CardActions from '@mui/material/CardActions';
import CardContent from '@mui/material/CardContent';
import UseToken from '../UseToken';


export default function CommentBox({selectEvent, selectCircle}) {
  const [italic, setItalic] = useState(false);
  const [fontWeight, setFontWeight] = useState('normal');
  const [anchorEl, setAnchorEl] = useState(null);
  const [comments, setCom] = useState([]);
  const [commentsLoaded, setCommentsLoaded] = useState(false);
  const [loaded, setLoaded] = useState(false);
  const [newComment, setNewComment] = useState();
  const [userId, setUserId] = useState(UserService.getUserId()); 
  const [firstName, setFirstName] = useState();
  const [lastName, setLastName] = useState();

  useEffect(() => {    
    if(loaded)
    {
      return;
    }
    setLoaded(true);  
    const f = async()=>{
      const tokenss = UseToken().token;
      const decodedToken = UserService.decodeJWT(tokenss);
      var datau = await IdentityService.userInfo(decodedToken.email);
      setLastName(datau.lastName);
      setFirstName(datau.firstName);
    }
    f();
  },[loaded]);

  const datap = async ()=>{
    if(selectCircle !== null && selectEvent.id !== undefined && !commentsLoaded)
    {
      var d = await CommentService.getCommentsByEventCircle(selectCircle, selectEvent?.id);
      if(d.length)
      {
        setCom(d);
        setCommentsLoaded(true)
      }
    }
  };
  
  const addComment = async () => {
    await CommentService.addComment(selectEvent?.id, selectCircle, userId, newComment, firstName + " " + lastName)
    setCommentsLoaded(false)
    setNewComment('');
    datap();
  };
  
  const removeComment = async (commentId) => {
    await CommentService.removeComment(commentId)
    setCommentsLoaded(false)
    datap();
  };

  if(UserService.IsAuthorized() === false)
  {
    return (<Box/>);
  }

  const ShowDeleteButton = (itemDatas) => {
    if (userId === itemDatas.userId) {
      return <> 
        <CardActions>
          <Button sx={{ ml: 'auto' }} onClick={()=>removeComment(itemDatas.id)}>Удалить</Button>
        </CardActions>
      </>;
    }
  }

  datap();
  return (
    <Box display="flex" justifyContent="center">
      <Paper sx={{width:'100%', alignContent:'center',textAlign: 'center'}}>
      <Grid container rowSpacing={1} columnSpacing={{ xs: 1, sm: 2, md: 3 }}>
        <Grid xs={12}>
          <Grid xs={12}>
            <Typography gutterBottom variant="h5" component="div">Коментарии</Typography>
          </Grid>
          {comments.map((itemDatas) => (
            <Box display="flex" justifyContent="center" >
            <Grid key={itemDatas.id} >
              <Card sx={{minWidth : 400}}>
                <CardContent>
                  <Typography gutterBottom variant="body2" display="flex" justifyContent="left">
                    {itemDatas.userName}
                  </Typography>
                  <Typography variant="body1" display="flex" justifyContent="left">
                    {itemDatas.message}                    
                  </Typography>
                </CardContent>
                {ShowDeleteButton(itemDatas)}
              </Card>
            </Grid>
            </Box>
          ))}
        </Grid>
        <Grid xs={12}>
            <Box display="flex" justifyContent="center" sx={{ display: 'flex','& > :not(style)': {m: 1,},}}>
              <FormControl sx={{width:'40%'}}>
                <FormLabel>
                  <Typography>Ваш комментарий:</Typography>
                </FormLabel>
                <Textarea placeholder="Текст комментария…" minRows={3} 
                    onChange={event => setNewComment(event.target.value)}
                    value = {newComment}
                    endDecorator={
                      <Box sx={{ display: 'flex', gap: 'var(--Textarea-paddingBlock)', 
                          pt: 'var(--Textarea-paddingBlock)', borderTop: '1px solid',
                          borderColor: 'divider', flex: 'auto',}}>
                        <Button sx={{ ml: 'auto' }} onClick={()=>addComment()}>Отправить</Button>
                      </Box>
                    } sx={{ minWidth: 300, fontWeight, fontStyle: italic ? 'italic' : 'initial',}}/>
              </FormControl>
            </Box>  
        </Grid>
      </Grid>
      </Paper>
    </Box>
  );
}