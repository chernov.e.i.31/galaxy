﻿using Identity.Models;
using Microsoft.AspNetCore.Identity;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Infrastructure;

namespace Identity.Data
{
    public class DbInitializer
    {
        public static void Initialize(AuthDbContext context)
        {
            context.Database.EnsureCreated();

            if (!context.Users.Any())
            {
                var userManager = context.GetService<UserManager<AppUser>>();

                var user = new AppUser
                {
                    UserName = "test@example.com",
                    Email = "test@example.com",
                    FirstName = "Иван",
                    LastName = "Иванов",
                    Gender = "Мужской",
                    DateOfBirthParsed = new DateTime(1990, 1, 1),
                    Location = "msk"
                };

                userManager.CreateAsync(user, "Password123").GetAwaiter().GetResult();

                var preference = new Pref { UserId = user.Id, Preference = "concert" };

                context.Pref.Add(preference);

                context.SaveChanges();
            }
        }
    }
}
