﻿using Identity.Data;
using Identity.Models;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;

namespace Identity.Controllers
{
    [ApiController]
    [Route("api/[controller]")]
    public class UsersController : ControllerBase
    {
        private readonly AuthDbContext _dbContext;

        public UsersController(AuthDbContext dbContext)
        {
            _dbContext = dbContext;
        }

        /// <summary>
        /// Получить всех пользователей
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        public async Task<ActionResult<IEnumerable<AppUser>>> GetAllUsers()
        {
            var users = await _dbContext.Users.ToListAsync();
            return Ok(users);
        }

        /// <summary>
        /// Получить список пользователей по имени (совпадение)
        /// </summary>
        /// <param name="name"></param>
        /// <returns></returns>
        [HttpGet("name/{name}")]
        public async Task<ActionResult<IEnumerable<AppUser>>> GetUsersByName(string name)
        {
            var users = await _dbContext.Users.Where(u => u.FirstName == name).ToListAsync();
            return Ok(users);
        }

        /// <summary>
        /// Получить пользователя по логину
        /// </summary>
        /// <param name="login"></param>
        /// <returns></returns>
        [HttpGet("login/{login}")]
        public async Task<ActionResult<AppUser>> GetUserByLogin(string login)
        {
            var user = await _dbContext.Users
                .Include(x=>x.Pref)
                .SingleOrDefaultAsync(u => u.UserName == login || u.Email == login);
            if (user == null)
            {
                return NotFound();
            }
            return Ok(user);
        }

        /// <summary>
        /// Получить имя пользователя по id
        /// </summary>
        /// <param name="login"></param>
        /// <returns></returns>
        [HttpGet("userName/{id}")]
        public async Task<ActionResult<string>> GetNameById(string id)
        {
            var user = await _dbContext.Users
                .Include(x => x.Pref)
                .SingleOrDefaultAsync(u => u.Id.Equals(id));
            if (user == null)
            {
                return NotFound();
            }
            return Ok(user.FirstName + " " + user.LastName);
        }

        /// <summary>
        /// Удалить пользователя по логину
        /// </summary>
        /// <param name="login"></param>
        /// <returns></returns>
        [HttpDelete("login/{login}")]
        public async Task<IActionResult> DeleteUserByLogin(string login)
        {
            var user = await _dbContext.Users.SingleOrDefaultAsync(u => u.UserName == login);
            if (user == null)
            {
                return NotFound();
            }

            _dbContext.Users.Remove(user);
            await _dbContext.SaveChangesAsync();

            return Ok(new { message = "Пользователь удален." });
        }

        /// <summary>
        /// Удалить всех пользователей
        /// </summary>
        /// <returns></returns>
        [HttpDelete]
        public async Task<IActionResult> DeleteAllUsers()
        {
            var users = await _dbContext.Users.ToListAsync();
            _dbContext.Users.RemoveRange(users);
            await _dbContext.SaveChangesAsync();

            return Ok(new { message = "Все пользователи удалены." });
        }
    }
}