﻿using FluentValidation;

namespace Identity.Models
{
    public class UpdateModelValidator : AbstractValidator<UpdateModel>
    {
        public UpdateModelValidator()
        {
            //.OverridePropertyName("Email")
            //.WithName("Email");
            RuleFor(model => model.Email).NotEmpty().WithMessage("Email является обязательным полем");
            /*RuleFor(model => model.Email).EmailAddress().WithMessage("Неверный формат электронной почты");*/

            RuleFor(model => model.FirstName).NotEmpty().WithMessage("Имя является обязательным полем");
            RuleFor(model => model.FirstName).Matches("^[a-zA-Zа-яА-Я]+$").WithMessage("Имя может содержать только буквы");

            RuleFor(model => model.LastName).NotEmpty().WithMessage("Фамилия является обязательным полем");
            RuleFor(model => model.LastName).Matches("^[a-zA-Zа-яА-Я]+$").WithMessage("Фамилия может содержать только буквы");

            /*RuleFor(model => model.Gender).NotEmpty().WithMessage("Пол является обязательным полем");*/

            RuleFor(model => model.DateOfBirth).NotEmpty().WithMessage("Дата рождения является обязательным полем");
            //RuleFor(model => model.DateOfBirth).Must(BeAValidDate).WithMessage("Неверный формат даты");

            RuleFor(model => model.Preferences).NotNull().WithMessage("Список предпочтений является обязательным полем");
        }
    }
}