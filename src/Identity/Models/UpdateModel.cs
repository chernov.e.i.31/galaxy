﻿using Swashbuckle.AspNetCore.Annotations;

namespace Identity.Models
{
    public class UpdateModel
    {
        public string? Username { get; set; }
        public string? Email { get; set; }
        public string? FirstName { get; set; }
        public string? LastName { get; set; }
        public string? Gender { get; set; }
        public string? Location { get; set; }
        public string? DateOfBirth { get; set; }
        public DateTime DateOfBirthParsed { get; set; }

        [SwaggerSchema(Required = new[] { "Preferences" })]
        public List<string>? Preferences { get; set; }
    }
}
