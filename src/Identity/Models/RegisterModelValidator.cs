﻿using FluentValidation;

namespace Identity.Models
{
    public class RegisterModelValidator : AbstractValidator<RegisterModel>
    {
        public RegisterModelValidator()
        {
            //.OverridePropertyName("Email")
            //.WithName("Email");
            RuleFor(model => model.Email).NotEmpty().WithMessage("Email является обязательным полем");
            /*RuleFor(model => model.Email).EmailAddress().WithMessage("Неверный формат электронной почты");*/

            RuleFor(model => model.Password).NotEmpty().WithMessage("Пароль является обязательным полем")
                .MinimumLength(3).WithMessage("Пароль должен содержать не менее 3 символов и/или букв");
                //.Must(password => password.Any(char.IsUpper)).WithMessage("Пароль должен содержать хотя бы одну заглавную букву");
                //.Must(password => password.Any(char.IsDigit)).WithMessage("Пароль должен содержать хотя бы одну цифру");
                //.Must(password => password.Any(char.IsSymbol)).WithMessage("Пароль должен содержать хотя бы один символ");

            RuleFor(model => model.FirstName).NotEmpty().WithMessage("Имя является обязательным полем");
            RuleFor(model => model.FirstName).Matches("^[a-zA-Zа-яА-Я]+$").WithMessage("Имя может содержать только буквы");

            RuleFor(model => model.LastName).NotEmpty().WithMessage("Фамилия является обязательным полем");
            RuleFor(model => model.LastName).Matches("^[a-zA-Zа-яА-Я]+$").WithMessage("Фамилия может содержать только буквы");

            /*RuleFor(model => model.Gender).NotEmpty().WithMessage("Пол является обязательным полем");*/

            RuleFor(model => model.DateOfBirth).NotEmpty().WithMessage("Дата рождения является обязательным полем");
            //RuleFor(model => model.DateOfBirth).Must(BeAValidDate).WithMessage("Неверный формат даты");

            RuleFor(model => model.Preferences).NotNull().WithMessage("Список предпочтений является обязательным полем");
        }

        private bool BeAValidDate(DateTime date)
        {
            return !date.Equals(default(DateTime));
        }
    }
}