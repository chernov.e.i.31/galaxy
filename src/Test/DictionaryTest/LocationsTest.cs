﻿using AutoFixture;
using AutoFixture.AutoMoq;
using FluentAssertions;
using Moq;
using SKE.EventManager.Domain;
using SKE.EventManager.Services.Abstractions;
using SKE.EventManager.Services.Implementations;
using SKE.EventManager.Services.Repositories.Abstractions;

namespace Test.DictionaryControllerTest
{
    public class LocationsTest
    {
        private readonly Mock<ICityRepository> _repoMock;
        private readonly IDictService _service;

        public LocationsTest()
        {
            var fixture = new Fixture().Customize(new AutoMoqCustomization());
            _repoMock = fixture.Freeze<Mock<ICityRepository>>();
            _service = fixture.Build<DictService>().OmitAutoProperties().Create();
        }

        [Fact]
        public async void GetLocationsAsyncTests_CheckResult_ReturnsEnumerableResult()
        {
            // Arrange
            var data = CreateData();
            _repoMock.Setup(repo => repo.GetFitleredAsync()).ReturnsAsync(data);

            // Act
            var result = await _service.GetCities();

            // Assert
            result.Should().BeAssignableTo<List<City>>();
            result.Count().Should().Be(data.Count);
        }

        private List<City> CreateData()
        {
            return new List<City> {
                new City() {
                    Id = Guid.Parse("dd08054d-859e-4ee8-ad8f-08993c1221da"),
                    Slug = "ekb",
                    Name = "Екатеринбург",
                    Timezone = "Asia/Yekaterinburg",
                    Language = "ru",
                    Currency = "RUB"
                },
                new City() {
                    Id = Guid.Parse("40e67056-03e7-4d3e-a811-6e2325017be7"),
                    Slug = "kzn",
                    Name = "Казань",
                    Timezone = "GMT+03:00",
                    Language = "ru",
                    Currency = "RUB"
                },
            };
        }
    }
}
