﻿using AutoFixture;
using AutoFixture.AutoMoq;
using FluentAssertions;
using Moq;
using SKE.EventManager.Domain;
using SKE.EventManager.Services.Abstractions;
using SKE.EventManager.Services.Implementations;
using SKE.EventManager.Services.Repositories.Abstractions;

namespace Test.DictionaryControllerTest
{
    public class CategoryEventTest
    {
        private readonly Mock<ICategoryEventRepository> _repoMock;
        private readonly IDictService _service;

        public CategoryEventTest()
        {
            var fixture = new Fixture().Customize(new AutoMoqCustomization());
            _repoMock = fixture.Freeze<Mock<ICategoryEventRepository>>();
            _service = fixture.Build<DictService>().OmitAutoProperties().Create();
        }

        [Fact]
        public async void GetCategoryEventAsyncTests_CheckResult_ReturnsEnumerableResult()
        {
            // Arrange
            var data = CreateData();
            _repoMock.Setup(repo => repo.GetFitleredAsync()).ReturnsAsync(data);

            // Act
            var result = await _service.GetCategoryEvents();

            // Assert
            result.Should().BeAssignableTo<List<CategoryEvent>>();
            result.Count().Should().Be(data.Count);
        }

        private List<CategoryEvent> CreateData()
        {
            return new List<CategoryEvent> {
                new CategoryEvent()
                {
                    Id = Guid.Parse("b6c6a9fd-56e9-4f01-bba0-a563832f194a"),
                    ExternalId = 1,
                    Slug = "concert",
                    Name = "Концерты"
                },
                new CategoryEvent()
                {
                    Id = Guid.Parse("1e5113e4-a968-430b-85b0-4069b8aeffea"),
                    ExternalId = 2,
                    Slug = "theater",
                    Name = "Спектакли"
                },
                new CategoryEvent()
                {
                    Id = Guid.Parse("bb21d933-6212-48fe-8a36-07602e916d0c"),
                    ExternalId = 3,
                    Slug = "education",
                    Name = "Обучение"
                }
            };
        }
    }
}
