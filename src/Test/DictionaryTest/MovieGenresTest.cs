﻿using AutoFixture;
using AutoFixture.AutoMoq;
using FluentAssertions;
using Moq;
using SKE.EventManager.Domain;
using SKE.EventManager.Services.Abstractions;
using SKE.EventManager.Services.Implementations;
using SKE.EventManager.Services.Repositories.Abstractions;

namespace Test.DictionaryControllerTest
{
    public class MovieGenresTest
    {
        private readonly Mock<IGenreRepository> _repoMock;
        private readonly IDictService _service;

        public MovieGenresTest()
        {
            var fixture = new Fixture().Customize(new AutoMoqCustomization());
            _repoMock = fixture.Freeze<Mock<IGenreRepository>>();
            _service = fixture.Build<DictService>().OmitAutoProperties().Create();
        }

        [Fact]
        public async void GetMovieGenresAsyncTests_CheckResult_ReturnsEnumerableResult()
        {
            // Arrange
            var data = CreateData();
            _repoMock.Setup(repo => repo.GetFitleredAsync()).ReturnsAsync(data);

            // Act
            var result = await _service.GetGenres();

            // Assert
            result.Should().BeAssignableTo<List<Genre>>();
            result.Count().Should().Be(data.Count);
        }

        private List<Genre> CreateData()
        {
            return new List<Genre> {
                new Genre() {
                    Id = Guid.Parse("761c22aa-3ad0-4259-a5e2-29a59c68e07e"),
                    Slug = "biographies",
                    Name = "биография"
                },
                new Genre() {
                    Id = Guid.Parse("e418ea3b-6708-499a-a66c-33277124f3f7"),
                    Slug = "drama",
                    Name = "драма"
                },
                new Genre() {
                    Id = Guid.Parse("c322212b-e7dd-4cf4-a7c3-296fc516d9d4"),
                    Slug = "cartoons",
                    Name = "Мультфильмы"
                }
            };
        }
    }
}
