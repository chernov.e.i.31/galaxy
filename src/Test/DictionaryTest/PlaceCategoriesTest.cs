﻿using AutoFixture;
using AutoFixture.AutoMoq;
using FluentAssertions;
using Moq;
using SKE.EventManager.Domain;
using SKE.EventManager.Services.Abstractions;
using SKE.EventManager.Services.Implementations;
using SKE.EventManager.Services.Repositories.Abstractions;

namespace Test.DictionaryControllerTest
{
    public class PlaceCategoriesTest
    {
        private readonly Mock<ICategoryPlaceRepository> _repoMock;
        private readonly IDictService _service;

        public PlaceCategoriesTest()
        {
            var fixture = new Fixture().Customize(new AutoMoqCustomization());
            _repoMock = fixture.Freeze<Mock<ICategoryPlaceRepository>>();
            _service = fixture.Build<DictService>().OmitAutoProperties().Create();
        }

        [Fact]
        public async void GetPlaceCategoriesAsyncTests_CheckResult_ReturnsEnumerableResult()
        {
            // Arrange
            var data = CreateData();
            _repoMock.Setup(repo => repo.GetFitleredAsync()).ReturnsAsync(data);

            // Act
            var result = await _service.GetCategoryPlaces();

            // Assert
            result.Should().BeAssignableTo<List<CategoryPlace>>();
            result.Count().Should().Be(data.Count);
        }

        private List<CategoryPlace> CreateData()
        {
            return new List<CategoryPlace> {
                new CategoryPlace() {
                    Id = Guid.Parse("5d512314-0adf-49bd-8b02-075616d92a32"),
                    ExternalId = 15,
                    Slug = "restaurants",
                    Name = "Рестораны и кафе"
                },
                new CategoryPlace() {
                    Id = Guid.Parse("8250c46f-e87c-4964-9199-4569c3566999"),
                    ExternalId = 17,
                    Slug = "anticafe",
                    Name = "Антикафе"
                },
                new CategoryPlace() {
                    Id = Guid.Parse("629783cc-d80c-47e4-b8dc-ea691e9920d9"),
                    ExternalId = 19,
                    Slug = "bar",
                    Name = "Бары и пабы"
                }
            };
        }
    }
}
