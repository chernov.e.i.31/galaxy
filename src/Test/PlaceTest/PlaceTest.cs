﻿using AutoFixture.AutoMoq;
using AutoFixture;
using Moq;
using SKE.EventManager.Services.Abstractions;
using SKE.EventManager.Services.Implementations;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using SKE.EventManager.Services.Repositories.Abstractions;
using SKE.EventManager.Services.Contracts;
using SKE.EventManager.Domain;
using FluentAssertions;

namespace Test.PlaceTest
{
    public class PlaceTest
    {
        private readonly Mock<IPlaceRepository> _repoMock;
        private readonly IPlaceService _service;

        public PlaceTest()
        {
            var fixture = new Fixture().Customize(new AutoMoqCustomization());
            _repoMock = fixture.Freeze<Mock<IPlaceRepository>>();
            _service = fixture.Build<PlaceService>().OmitAutoProperties().Create();
        }


        [Fact]
        public async void GetPlaceAsyncTests_CheckCategories_ReturnEnumerable()
        {
            // Arrange
            var data = CreateData();
            var param = new PlaceFilterDTO() { IncludeCategories = true };
            _repoMock.Setup(repo => repo.GetFitleredAsync(param)).ReturnsAsync(data);

            // Act
            var result = await _service.GetPlaces(param);
            var element = result.FirstOrDefault(x => x.Id == Guid.Parse("01a24bd6-4012-4e65-98e2-329b891641d6"));

            // Assert
            result.Should().BeAssignableTo<List<Place>>();
            element.Should().NotBeNull();
            element.Categories.Should().NotBeNull();
            element.Categories.Count().Should().NotBe(0);
        }

        [Fact]
        public async void GetPlaceAsyncTests_CheckImages_ReturnEnumerable()
        {
            // Arrange
            var data = CreateData();
            var param = new PlaceFilterDTO() { IncludeImages = true };
            _repoMock.Setup(repo => repo.GetFitleredAsync(param)).ReturnsAsync(data);

            // Act
            var result = await _service.GetPlaces(param);
            var element = result.FirstOrDefault(x => x.Id == Guid.Parse("01a24bd6-4012-4e65-98e2-329b891641d6"));

            // Assert
            result.Should().BeAssignableTo<List<Place>>();
            element.Should().NotBeNull();
            element.Images.Should().NotBeNull();
            element.Images.Count().Should().NotBe(0);
        }

        [Fact]
        public async void GetPlaceAsyncTests_CheckTags_ReturnEnumerable()
        {
            // Arrange
            var data = CreateData();
            var param = new PlaceFilterDTO() { IncludeTags = true };
            _repoMock.Setup(repo => repo.GetFitleredAsync(param)).ReturnsAsync(data);

            // Act
            var result = await _service.GetPlaces(param);
            var element = result.FirstOrDefault(x => x.Id == Guid.Parse("01a24bd6-4012-4e65-98e2-329b891641d6"));

            // Assert
            result.Should().BeAssignableTo<List<Place>>();
            element.Should().NotBeNull();
            element.Tags.Should().NotBeNull();
            element.Tags.Count().Should().NotBe(0);
        }

        private List<Place> CreateData()
        {
            return new List<Place>
            {
                new Place {
                    Id = Guid.Parse("01a24bd6-4012-4e65-98e2-329b891641d6"),
                    ExternalId = 32366,
                    Title = "лофт-пространство Party Time",
                    Slug = "antikafe-party-time",
                    Address = "Думская ул., д. 4",
                    TimeTable = "ежедневно 12:00–0:00",
                    Phone = "+7 812 220-96-67",
                    IsStub = false,
                    BodyText = "Party Time",
                    Description = "Собраться весёлой компанией",
                    SiteUrl = "https://kudago.com/spb/place/antikafe-party-time/",
                    ForeignUrl = "https://partytimespb.ru?utm_source=kudago",
                    SubWay = "Гостиный двор, Невский Проспект",
                    FavoritesCount = 50,
                    CommentsCount = 3,
                    IsClosed = false,
                    ShortTitle = "Party Time",
                    Images = new List<Image> {
                        new Image {
                            Id = Guid.Parse("017deb5c-b70a-44c5-8a34-ca56adb84042"),
                            ImageUrl = "https://kudago.com/media/images/place/73/98/7398e5e3c2c0b9be529f958946338986.jpg",
                            Thumbnails = new Thumbnail {
                                Id = Guid.Parse("f37bfb5d-94be-4798-9898-edb2afe4e077"),
                                Url640x384 = "https://kudago.com/media/thumbs/640x384/images/place/73/98/7398e5e3c2c0b9be529f958946338986.jpg",
                                Url144x96 = "https://kudago.com/media/thumbs/144x96/images/place/73/98/7398e5e3c2c0b9be529f958946338986.jpg"
                            },
                        },
                        new Image {
                            Id = Guid.Parse("03a03c0f-047c-4d1a-8ef5-c7491442389a"),
                            ImageUrl = "https://kudago.com/media/images/place/40/c4/40c4fe7edbacc2dad3cc4eafaf6a78b7.jpg",
                            Thumbnails = new Thumbnail {
                                Id = Guid.Parse("c3d05f26-0b32-4b0f-b450-8b69b8e10fbc"),
                                Url640x384 = "https://kudago.com/media/thumbs/640x384/images/place/40/c4/40c4fe7edbacc2dad3cc4eafaf6a78b7.jpg",
                                Url144x96 = "https://kudago.com/media/thumbs/144x96/images/place/40/c4/40c4fe7edbacc2dad3cc4eafaf6a78b7.jpg"
                            },
                        },
                    },
                    Categories = new List<CategoryPlace> {
                        new CategoryPlace {
                            Id = Guid.Parse("083ef904-e169-4b1c-ad09-d16f195cdbe0"),
                            ExternalId = 89,
                            Slug = "amusement",
                            Name = "Развлечения"
                        },
                        new CategoryPlace {
                            Id = Guid.Parse("8250c46f-e87c-4964-9199-4569c3566999"),
                            ExternalId = 17,
                            Slug = "anticafe",
                            Name = "Антикафе"
                            },
                    },
                    Tags = new List<Tag> {
                        new Tag {
                            Id = Guid.Parse("0a3d0e9b-39f2-4014-a3aa-46692b2066b1"),
                            Name = "с друзьями"
                        },
                        new Tag {
                            Id = Guid.Parse("12dbd0d8-8aea-4fa8-acb9-51bdf5cb571b"),
                            Name = "отметить день рождения"
                        },
                    }
                }
            };
        }
    }
}
