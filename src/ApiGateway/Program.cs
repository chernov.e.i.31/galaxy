using Ocelot.DependencyInjection;
using Ocelot.Middleware;

var builder = WebApplication.CreateBuilder(args);
builder.Configuration.SetBasePath(builder.Environment.ContentRootPath)
    .AddJsonFile("ocelot.json", optional: false, reloadOnChange: true)
    .AddEnvironmentVariables();

builder.Logging.AddConfiguration(builder.Configuration.GetSection("Logging"));
builder.Logging.AddConsole();

builder.Services.AddOcelot(builder.Configuration);

// ��������� Kestrel ������� ��� ������������� ����������� ������������
/*builder.WebHost.ConfigureKestrel(options =>
{
    options.ConfigureHttpsDefaults(httpsOptions =>
    {
        httpsOptions.ServerCertificate = CertificateLoader.LoadFromStoreCert(
            "localhost", "My", StoreLocation.CurrentUser,
            allowInvalid: true);
    });
});*/

var app = builder.Build();

app.Use(async (context, next) =>
{
    var logger = context.RequestServices.GetRequiredService<ILogger<Program>>();
    logger.LogDebug("-------------------------------------------------------------------------------");
    logger.LogDebug("Request: {Method} {Path} {QueryString} {Headers}", context.Request.Method, context.Request.Path, context.Request.QueryString, context.Request.Headers);
    await next();
    logger.LogDebug("Response: {StatusCode} {Headers}", context.Response.StatusCode, context.Response.Headers);
    logger.LogDebug("-------------------------------------------------------------------------------");
});

await app.UseOcelot();

app.Run();