﻿namespace SKE.Activity.Middlewares
{
    /// <summary>
    /// Midlleware логирования.
    /// </summary>
    public class LoggingMidlleware
    {
        private readonly RequestDelegate _next;
        private readonly ILogger<LoggingMidlleware> _logger;
        private readonly IWebHostEnvironment _env;

        /// <summary>
        /// ctor логирования.
        /// </summary>
        public LoggingMidlleware(
                    RequestDelegate next,
                    ILogger<LoggingMidlleware> logger,
                    IWebHostEnvironment env)
        {
            _logger = logger;
            _next = next;
            _env = env;
        }
        /// <summary>
        /// асинхронное выполнение.
        /// </summary>
        public async Task InvokeAsync(HttpContext httpContext)
        {
            await _next(httpContext);
            _logger.Log(
                LogLevel.Information,
                "Request method: {0}; Request path: {1}; Request query: {2}; Request querystring: {3}; Response status code: {4}; Environment: {5}",
                httpContext.Request?.Method,
                httpContext.Request?.Path,
                httpContext.Request?.Query,
                httpContext.Request?.QueryString,
                httpContext.Response?.StatusCode,
                _env.EnvironmentName);
        }
    }
}
