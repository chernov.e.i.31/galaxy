﻿using SKE.CoreLib.Core;

namespace SKE.Activity.Domain
{
    /// <summary>
    /// Модель комментария
    /// </summary>
    public class Comment : BaseEntity
    {
        /// <summary>
        /// ссылка на событие
        /// </summary>
        public Guid EventId { get; set; }
        /// <summary>
        /// ссылка на пользователя
        /// </summary>
        public Guid UserId { get; set; }
        /// <summary>
        /// ссылка на круг
        /// </summary>
        public Guid CircleId { get; set; }
        /// <summary>
        /// дата время публикации
        /// </summary>
        public DateTime PublicationDatetime { get; set; }
        /// <summary>
        /// имя пользователя
        /// </summary>
        public string UserName { get; set; } = string.Empty;
        /// <summary>
        /// сообщение
        /// </summary>
        public string Message { get; set; } = string.Empty;
    }
}
