﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SKE.Activity.Domain
{
    public class CommentModel
    {
        public Guid circeId { get; set; }
        public Guid eventId { get; set; }
        public Guid userId { get; set; }
        public string message { get; set; }
        public string userName { get; set; }
    }
}
