﻿namespace SKE.Activity.Services.Contracts
{
    public class LikeDTO
    {
        public Guid Id { get; set; }
        /// <summary>
        /// дата время публикации
        /// </summary>
        public DateTime PublicationDatetime { get; set; }
        /// <summary>
        /// сообщение
        /// </summary>
        public bool isLike { get; set; } = false;
    }
}