﻿using SKE.KudaGo.Domain.Model;

namespace SKE.KudaGo.Services.Repositories.Abstractions
{
    /// <summary>
    /// Репозиторий работы с мест
    /// </summary>
    public interface IPlaceRepository : IRepository<KudaGoResponse<Place>>
    {
        /// <summary>
        /// Запросить все сущности 
        /// </summary>
        /// <returns> массив сущностей</returns>
        IAsyncEnumerable<IEnumerable<Place>> GetAllAsync();
    }
}