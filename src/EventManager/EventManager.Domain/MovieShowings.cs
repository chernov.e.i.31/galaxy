﻿using SKE.CoreLib.Core;

namespace SKE.EventManager.Domain
{
    /// <summary>
    /// Связь многие ко многим фильмы и места, включена информация о показах
    /// </summary>
    public class MovieShowings : BaseEntityExtension
    {
        /// <summary>
        /// время показа
        /// </summary>
        public DateTime ShowingsDate { get; set; }
        /// <summary>
        /// признак 3D
        /// </summary>
        public bool Is3D { get; set; }
        /// <summary>
        /// признак IMAX
        /// </summary>
        public bool IsImax { get; set; }
        /// <summary>
        /// признак 4D
        /// </summary>
        public bool Is4D { get; set; }
        /// <summary>
        /// язык оригинала
        /// </summary>
        public bool OriginalLanguage { get; set; }
        /// <summary>
        /// стоимость
        /// </summary>
        public string? Price { get; set; }

        /// <summary>
        /// идентификатор фильма
        /// </summary>
        public Guid MovieId { get; set; }
        /// <summary>
        /// фильм
        /// </summary>
        public Movie Movie { get; set; }

        /// <summary>
        /// идентификатор места
        /// </summary>
        public Guid PlaceId { get; set; }
        /// <summary>
        /// место
        /// </summary>
        public Place Place { get; set; }
    }
}
