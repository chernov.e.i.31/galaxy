﻿
using SKE.CoreLib.Core;

namespace SKE.EventManager.Domain
{
    /// <summary>
    /// Места
    /// </summary>
    public class Place : BaseEntityExtension
    {
        /// <summary>
        /// название
        /// </summary>
        public string Title { get; set; }
        /// <summary>
        /// уникальное поле сущности
        /// </summary>
        public string Slug { get; set; }
        /// <summary>
        /// адрес
        /// </summary>
        public string Address { get; set; }
        /// <summary>
        /// расписание
        /// </summary>
        public string TimeTable { get; set; }
        /// <summary>
        /// телефон
        /// </summary>
        public string Phone { get; set; }
        /// <summary>
        /// является ли заглушкой
        /// </summary>
        public bool IsStub { get; set; }
        /// <summary>
        /// полное описание
        /// </summary>
        public string BodyText { get; set; }
        /// <summary>
        /// описание
        /// </summary>
        public string Description { get; set; }
        /// <summary>
        /// адрес места на сайте kudago.com
        /// </summary>
        public string SiteUrl { get; set; }
        /// <summary>
        /// сайт места
        /// </summary>
        public string ForeignUrl { get; set; }
        /// <summary>
        /// метро рядом
        /// </summary>
        public string SubWay { get; set; }
        /// <summary>
        /// число пользователей, добавивших место в избранное
        /// </summary>
        public int FavoritesCount { get; set; }
        /// <summary>
        /// число комментариев
        /// </summary>
        public int CommentsCount { get; set; }
        /// <summary>
        /// закрыто ли место
        /// </summary>
        public bool IsClosed { get; set; }
        /// <summary>
        /// короткое название
        /// </summary>
        public string ShortTitle { get; set; }

        /// <summary>
        /// идентификатор города
        /// </summary>
        public Guid LocationId { get; set; }
        /// <summary>
        /// город
        /// </summary>
        public City Location { get; set; }
        /// <summary>
        /// координаты места
        /// </summary>
        public Coordinate Coords { get; set; }
        /// <summary>
        /// галерея места
        /// </summary>
        public ICollection<Image> Images { get; set; }
        /// <summary>
        /// связь категорий с местами
        /// </summary>
        public ICollection<CategoryToPlace> CategoryToPlaces { get; set; }
        /// <summary>
        /// список категорий
        /// </summary>
        public ICollection<CategoryPlace> Categories { get; set; }
        /// <summary>
        /// связь тэгов с местами
        /// </summary>
        public ICollection<TagToPlace> TagToPlaces { get; set; }
        /// <summary>
        /// тэги места
        /// </summary>
        public ICollection<Tag> Tags { get; set; }
        /// <summary>
        /// События
        /// </summary>
        public ICollection<Event> Events { get; set; }
        /// <summary>
        /// список показа фильмов
        /// </summary>
        public ICollection<MovieShowings> MovieShowings { get; set; }
        /// <summary>
        /// список фильмов
        /// </summary>
        public ICollection<Movie> Movies { get; set; }

        public Place()
        {
            Categories = new List<CategoryPlace>();
            Tags = new List<Tag>();
        }
    }
}
