﻿using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.DependencyInjection;

namespace SKE.EventManager.DataBaseContext
{
    public static class EntityFrameworkInstaller
    {
        public static IServiceCollection ConfigureContext(this IServiceCollection services,
            string connectionString)
        {
            services.AddDbContext<DataContext>(optionsBuilder
                => optionsBuilder
                .UseNpgsql(connectionString,
                    sqlServerOptions => sqlServerOptions.CommandTimeout(60)));
            AppContext.SetSwitch("Npgsql.EnableLegacyTimestampBehavior", true);
            //services.AddHealthChecks()
            //    .AddDbContextCheck<DataContext>();

            return services;
        }
    }
}
