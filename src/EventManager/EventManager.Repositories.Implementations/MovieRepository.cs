﻿using Microsoft.EntityFrameworkCore;
using SKE.Core.Repositories.Implementations;
using SKE.EventManager.DataBaseContext;
using SKE.EventManager.Domain;
using SKE.EventManager.Services.Contracts;
using SKE.EventManager.Services.Repositories.Abstractions;

namespace SKE.EventManager.Repositories.Implementations
{
    public class MovieRepository : Repository<Movie, Guid>, IMovieRepository
    {
        private int _page = 1;
        private int _pageSize = 20;
        public MovieRepository(DataContext context) : base(context)
        {
        }

        public Task<List<Movie>> GetFitleredAsync(MovieFilterDTO filterDTO)
        {
            var result = GetAll();
            if (filterDTO.IncludeGenres ?? false)
            {
                result = result.Include(x => x.Genres);
            }
            if (filterDTO.IncludePoster ?? false)
            {
                result = result.Include(x => x.Poster);
            }
            if (filterDTO.IncludeImages ?? false)
            {
                result = result.Include(x => x.Images)
                    .ThenInclude(x => x.Thumbnails);
                result = result.Include(x => x.Images)
                    .ThenInclude(x => x.Source);
            }

            if (filterDTO.Genres is not null)
            {
                result = result.Include(x => x.Genres)
                    .Where(x => x.Genres.Any(y => filterDTO.Genres.Contains(y.Name)));
            }

            if (filterDTO.PlaceId.HasValue)
            {
                result = result.Include(x => x.Places)
                    .Where(x => x.Places.Any(y => y.Id == filterDTO.PlaceId));
            }
            if (filterDTO.DateTime.HasValue)
            {
                result = result.Include(x => x.MovieShowings.Where(x => x.ShowingsDate>= filterDTO.DateTime))
                    .Where(x => x.MovieShowings.Any(x => x.ShowingsDate >= filterDTO.DateTime));
            }

            if (filterDTO.PageSize.HasValue)
            {
                _pageSize = filterDTO.PageSize.Value <= 0 ? _pageSize : filterDTO.PageSize.Value;
            }
            if (filterDTO.Page.HasValue)
            {
                _page = filterDTO.Page.Value <= 0 ? _page : filterDTO.Page.Value;
            }

            return result
                .Take(_pageSize)
                .Skip((_page - 1) * _pageSize)
                .ToListAsync();
        }
    }
}