﻿using Microsoft.EntityFrameworkCore;
using SKE.Core.Repositories.Implementations;
using SKE.EventManager.DataBaseContext;
using SKE.EventManager.Domain;
using SKE.EventManager.Services.Contracts;
using SKE.EventManager.Services.Repositories.Abstractions;

namespace SKE.EventManager.Repositories.Implementations
{
    public class EventRepository : Repository<Event, Guid>, IEventRepository
    {
        private int _page = 0;
        private int _pageSize = 20;

        public EventRepository(DataContext context) : base(context)
        {
        }

        public Task<List<Event>> GetFitleredAsync(EventFilterDTO filterDTO)
        {
            var result = GetAll();
            if (filterDTO.IncludeCategories ?? false)
            {
                result = result.Include(x => x.Categories);
            }
            if (filterDTO.IncludeTags ?? false)
            {
                result = result.Include(x => x.Tags);
            }
            if (filterDTO.IncludeDates ?? false)
            {
                if (filterDTO.DateTime.HasValue)
                {
                    result = result
                        .Include(x => x.Dates.Where(x => x.Start >= filterDTO.DateTime || x.End >= filterDTO.DateTime || !filterDTO.DateTime.HasValue));
                }
                else
                {
                    var currentDate = DateTime.Now.Date;
                    result = result
                        .Include(x => x.Dates.Where(x => x.Start >= currentDate || x.End >= currentDate))
                        .Where(x => x.Dates.Any(y => y.Start >= currentDate || y.End >= currentDate));
                }
            }
            if (filterDTO.IncludePlace ?? false)
            {
                result = result.Include(x => x.Place);
            }
            if (filterDTO.IncludeImages ?? false)
            {
                result = result.Include(x => x.Images)
                    .ThenInclude(x => x.Thumbnails);
            }

            if (filterDTO.Tags is not null && filterDTO.Tags.Any())
            {
                result = result.Include(x => x.Tags)
                    .Where(x => x.Tags.Any(y => filterDTO.Tags.Contains(y.Name)));
            }
            if (filterDTO.Categories is not null && filterDTO.Categories.Any())
            {
                result = result.Include(x=>x.Categories)
                    .Where(x => x.Categories.Any(y => filterDTO.Categories.Contains(y.Slug))); 
            }
            if (filterDTO.EventsId is not null && filterDTO.EventsId.Any())
            {
                result = result
                    .Where(x => filterDTO.EventsId.Contains(x.Id));
            }
            if (!String.IsNullOrEmpty(filterDTO.CityId))
            {
                result = result
                    .Where(x => x.Location.Slug.Equals(filterDTO.CityId));
            }
            if (filterDTO.PlaceId.HasValue)
            {
                result = result
                    .Where(x => x.PlaceId == filterDTO.PlaceId);
            }
            if (filterDTO.DateTime.HasValue)
            {
                result = result.Include(x => x.Dates.Where(x=> x.Start >= filterDTO.DateTime || x.End >= filterDTO.DateTime))
                    .Where(x => x.Dates.Any(y => y.Start >= filterDTO.DateTime || y.End >= filterDTO.DateTime));
            }
            if (filterDTO.IncludeLocation ?? false)
            {
                result = result.Include(x => x.Location);
            }


            if (filterDTO.PageSize.HasValue)
            {
                _pageSize = filterDTO.PageSize.Value <= 0 ? _pageSize : filterDTO.PageSize.Value;
            }
            if (filterDTO.Page.HasValue)
            {
                _page = filterDTO.Page.Value < 0 ? _page : filterDTO.Page.Value;
            }

            return result
                .Skip(_page * _pageSize)
                .Take(_pageSize)
                .ToListAsync();
        }
    }
}