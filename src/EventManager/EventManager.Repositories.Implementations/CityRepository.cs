﻿using Microsoft.EntityFrameworkCore;
using SKE.Core.Repositories.Implementations;
using SKE.EventManager.DataBaseContext;
using SKE.EventManager.Domain;
using SKE.EventManager.Services.Repositories.Abstractions;

namespace SKE.EventManager.Repositories.Implementations
{
    public class CityRepository : Repository<City, Guid>, ICityRepository
    {
        public CityRepository(DataContext context) : base(context)
        {
        }

        public Task<List<City>> GetFitleredAsync()
        {
            return GetAll().ToListAsync();
        }
    }
}