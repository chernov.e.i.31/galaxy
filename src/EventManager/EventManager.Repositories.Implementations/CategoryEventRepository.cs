﻿using Microsoft.EntityFrameworkCore;
using SKE.Core.Repositories.Implementations;
using SKE.EventManager.DataBaseContext;
using SKE.EventManager.Domain;
using SKE.EventManager.Services.Repositories.Abstractions;

namespace SKE.EventManager.Repositories.Implementations
{
    public class CategoryEventRepository : Repository<CategoryEvent, Guid>, ICategoryEventRepository
    {
        public CategoryEventRepository(DataContext context) : base(context)
        {
        }

        public Task<List<CategoryEvent>> GetFitleredAsync()
        {
            return GetAll().ToListAsync();
        }
    }
}