﻿using Microsoft.EntityFrameworkCore;
using SKE.Core.Repositories.Implementations;
using SKE.EventManager.DataBaseContext;
using SKE.EventManager.Domain;
using SKE.EventManager.Services.Repositories.Abstractions;

namespace SKE.EventManager.Repositories.Implementations
{
    public class CategoryPlaceRepository : Repository<CategoryPlace, Guid>, ICategoryPlaceRepository
    {
        public CategoryPlaceRepository(DataContext context) : base(context)
        {
        }

        public Task<List<CategoryPlace>> GetFitleredAsync()
        {
            return GetAll().ToListAsync();
        }
    }
}