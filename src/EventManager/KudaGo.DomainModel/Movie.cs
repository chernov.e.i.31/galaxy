﻿using Newtonsoft.Json;

namespace SKE.KudaGo.Domain.Model
{
    public class Movie
    {
        private uint _dateUnix;
        private DateTime _date;

        [JsonProperty("id")]
        public int Id { get; set; }
        [JsonProperty("site_url")]
        public string SiteUrl { get; set; }
        [JsonProperty("publication_date")]
        public uint PublicationDateUnix
        {
            get { return _dateUnix; }
            set
            {
                _dateUnix = value;
                _date = new DateTime(1970, 1, 1)
                    .AddSeconds(_dateUnix);
            }
        }
        [JsonIgnore]
        public DateTime PublicationDate { get { return _date; } set { _date = value; } }
        [JsonProperty("slug")]
        public string Slug { get; set; }
        [JsonProperty("title")]
        public string Title { get; set; }
        [JsonProperty("description")]
        public string Description { get; set; }
        [JsonProperty("body_text")]
        public string BodyText { get; set; }
        [JsonProperty("is_editors_choice")]
        public bool IsEditorsChoice { get; set; }
        [JsonProperty("favorites_count")]
        public int FavoritesCount { get; set; }
        [JsonProperty("genres")]
        public List<Genre> Genres { get; set; }
        [JsonProperty("comments_count")]
        public int CommentsCount { get; set; }
        [JsonProperty("original_title")]
        public string OriginalTitle { get; set; }
        [JsonProperty("locale")]
        public string Locale { get; set; }
        [JsonProperty("country")]
        public string Country { get; set; }
        [JsonProperty("year")]
        public int Year { get; set; }
        [JsonProperty("language")]
        public string Language { get; set; }
        [JsonProperty("running_time")]
        public int? RunningTime { get; set; }
        [JsonProperty("budget_currency")]
        public string BudgetCurrency { get; set; }
        [JsonProperty("budget")]
        public decimal Budget { get; set; }
        [JsonProperty("mpaa_rating")]
        public string MpaaRating { get; set; }
        [JsonProperty("age_restriction")]
        public string AgeRestriction { get; set; }
        [JsonProperty("stars")]
        public string Stars { get; set; }
        [JsonProperty("director")]
        public string Director { get; set; }
        [JsonProperty("writer")]
        public string Writer { get; set; }
        [JsonProperty("awards")]
        public string Awards { get; set; }
        [JsonProperty("trailer")]
        public string Trailer { get; set; }
        [JsonProperty("images")]
        public List<Image> Images { get; set; }
        [JsonProperty("poster")]
        public Image Poster { get; set; }
        [JsonProperty("url")]
        public string Url { get; set; }
        [JsonProperty("imdb_url")]
        public string ImdbUrl { get; set; }
        [JsonProperty("imdb_rating")]
        public decimal? ImdbRating { get; set; }

        public Movie()
        {
            Genres = new();
            Images = new();
        }
    }
}
