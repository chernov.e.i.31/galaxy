﻿using Newtonsoft.Json;

namespace SKE.KudaGo.Domain.Model
{
    public class CategoryEvent
    {
        [JsonProperty("id")]
        public int Id { get; set; }
        [JsonProperty("slug")]
        public string Slug { get; set; }
        [JsonProperty("name")]
        public string Name { get; set; }
    }
}
