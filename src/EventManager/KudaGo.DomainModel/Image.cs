﻿using Newtonsoft.Json;

namespace SKE.KudaGo.Domain.Model
{
    public class Image
    {
        [JsonProperty("image")]
        public string ImageUrl { get; set; }
        [JsonProperty("thumbnails")]
        public Thumbnail? Thumbnails { get; set; }
        [JsonProperty("source")]
        public Source Source { get; set; }
    }
}
