﻿using Newtonsoft.Json;

namespace SKE.KudaGo.Domain.Model
{
    public class Thumbnail
    {
        [JsonProperty("640x384")]
        public string Url640x384 { get; set; }
        [JsonProperty("144x96")]
        public string Url144x96 { get; set; }
    }
}
