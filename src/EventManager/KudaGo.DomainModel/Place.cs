﻿using Newtonsoft.Json;

namespace SKE.KudaGo.Domain.Model
{
    public class Place
    {
        [JsonProperty("id")]
        public int Id { get; set; }
        [JsonProperty("title")]
        public string Title { get; set; }
        [JsonProperty("slug")]
        public string Slug { get; set; }
        [JsonProperty("address")]
        public string Address { get; set; }
        [JsonProperty("timetable")]
        public string TimeTable { get; set; }
        [JsonProperty("phone")]
        public string Phone { get; set; }
        [JsonProperty("is_stub")]
        public bool IsStub { get; set; }
        [JsonProperty("body_text")]
        public string BodyText { get; set; }
        [JsonProperty("description")]
        public string Description { get; set; }
        [JsonProperty("site_url")]
        public string SiteUrl { get; set; }
        [JsonProperty("foreign_url")]
        public string ForeignUrl { get; set; }
        [JsonProperty("coords")]
        public Coordinates Coords { get; set; }
        [JsonProperty("subway")]
        public string SubWay { get; set; }
        [JsonProperty("favorites_count")]
        public int FavoritesCount { get; set; }
        [JsonProperty("images")]
        public List<Image> Images { get; set; }
        [JsonProperty("comments_count")]
        public int CommentsCount { get; set; }
        [JsonProperty("is_closed")]
        public bool IsClosed { get; set; }
        [JsonProperty("categories")]
        public List<string> Categories { get; set; }
        [JsonProperty("short_title")]
        public string ShortTitle { get; set; }
        [JsonProperty("tags")]
        public List<string> Tags { get; set; }
        [JsonProperty("location")]
        public string Location { get; set; }

        public Place()
        {
            Tags = new();
            Images = new();
        }
    }
}
