﻿namespace SKE.EventManager.Services.Contracts
{
    /// <summary>
    /// DTO Фильтра фильмов
    /// </summary>
    public class MovieFilterDTO
    {
        public bool? IncludeGenres { get; set; } = false;
        public bool? IncludeImages { get; set; } = false;
        public bool? IncludePoster { get; set; } = false;

        public List<string>? Genres { get; set; } = null;
        public Guid? PlaceId { get; set; } = null;
        public DateTime? DateTime { get; set; } = null;

        public int? PageSize { get; set; } = 100;
        public int? Page { get; set; } = 1;
    }
}