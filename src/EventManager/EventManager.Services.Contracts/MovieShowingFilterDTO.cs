﻿namespace SKE.EventManager.Services.Contracts
{
    /// <summary>
    /// DTO Фильтра фильмов
    /// </summary>
    public class MovieShowingFilterDTO
    {
        public int? movieExternalId { get; set; }
    }
}