﻿using AutoMapper;
using SKE.Core.gRPCLib;
using SKE.EventManager.Services.Contracts;

namespace SKE.EventManager.Mapping
{
    /// <summary>
    /// Фильтр для событий.
    /// </summary>
    public class EventFilterDTOMappingsProfile : Profile
    {
        /// <summary>
        /// Конструктор
        /// </summary>
        public EventFilterDTOMappingsProfile()
        {
            CreateMap<EventFilterGRPC, EventFilterDTO>()
                .ForMember(x => x.EventsId, map => map.MapFrom(s => s.EventsId.Select(x => Guid.Parse(x))))
                .ForMember(x => x.CityId, map => map.MapFrom(s => string.IsNullOrEmpty(s.CityId) ? null : s.CityId))
                .ForMember(x => x.PlaceId, map => map.MapFrom(s => string.IsNullOrEmpty(s.PlaceId) ? null : (Guid?)Guid.Parse(s.PlaceId)));
        }
    }
}
