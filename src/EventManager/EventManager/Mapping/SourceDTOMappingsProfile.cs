﻿using AutoMapper;
using SKE.Core.DTO.EventManager;
using SKE.Core.gRPCLib.Shared;
using EF = SKE.EventManager.Domain;

namespace SKE.EventManager.Mapping
{
    /// <summary>
    /// Профиль автомаппера для сущности города.
    /// </summary>
    public class SourceDTOMappingsProfile : Profile
    {
        /// <summary>
        /// ctor
        /// </summary>
        public SourceDTOMappingsProfile()
        {
            CreateMap<SourceDTO, EF.Source>();
            CreateMap<EF.Source, SourceDTO>();

            CreateMap<EF.Source, SourceGRPC>();
        }
    }
}
