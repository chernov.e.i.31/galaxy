﻿using AutoMapper;
using SKE.Core.DTO.EventManager;
using SKE.Core.gRPCLib.Shared;
using EF = SKE.EventManager.Domain;

namespace SKE.EventManager.Mapping
{
    /// <summary>
    /// Профиль автомаппера для сущности города.
    /// </summary>
    public class CoordinatesDTOMappingsProfile : Profile
    {
        /// <summary>
        /// ctor
        /// </summary>
        public CoordinatesDTOMappingsProfile()
        {
            CreateMap<CoordinatesDTO, EF.Coordinate>();
            CreateMap<EF.Coordinate, CoordinatesDTO>();

            CreateMap<EF.Coordinate, CoordinatesGRPC>()
                .ForMember(x => x.Id, map => map.MapFrom(s => s.Id.ToString()));
        }
    }
}
