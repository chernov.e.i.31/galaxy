﻿using AutoMapper;
using Google.Protobuf.WellKnownTypes;
using SKE.Core.DTO.EventManager;
using SKE.Core.gRPCLib;
using EF = SKE.EventManager.Domain;

namespace SKE.EventManager.Mapping
{
    /// <summary>
    /// Профиль автомаппера для сущности Даты.
    /// </summary>
    public class DatesDTOMappingsProfile : Profile
    {
        /// <summary>
        /// ctor
        /// </summary>
        public DatesDTOMappingsProfile()
        {
            CreateMap<DatePeriodDTO, EF.DatePeriod>();
            CreateMap<EF.DatePeriod, DatePeriodDTO>();

            CreateMap<EF.DatePeriod, DatePeriodGRPC>()
                .ForMember(x => x.Start, map => map.MapFrom(s => Timestamp.FromDateTime(DateTime.SpecifyKind(s.Start, DateTimeKind.Utc))))
                .ForMember(x => x.End, map => map.MapFrom(s => Timestamp.FromDateTime(DateTime.SpecifyKind(s.End, DateTimeKind.Utc))));
        }
    }
}
