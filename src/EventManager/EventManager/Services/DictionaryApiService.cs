﻿using AutoMapper;
using Google.Protobuf.WellKnownTypes;
using Grpc.Core;
using Microsoft.AspNetCore.Authorization;
using Microsoft.Extensions.Caching.Distributed;
using SKE.Core.gRPCLib;
using SKE.Core.gRPCLib.Shared;
using SKE.EventManager.Controllers;
using SKE.EventManager.Domain;
using SKE.EventManager.Services.Abstractions;
using System.Text.Json;

namespace SKE.EventManager.Services
{
    /// <summary>
    /// Контроллер gRPC словарей.
    /// </summary>
    public class DictionaryApiService : DictionaryGRPC.DictionaryGRPCBase
    {
        private IDictService _service;
        private IMapper _mapper;
        private readonly ILogger<DictionaryController> _logger;
        private readonly IDistributedCache _distributedCache;

        /// <summary>
        /// ctor Контроллер gRPC словарей.
        /// </summary>
        public DictionaryApiService(IDictService service,
            IMapper mapper,
            ILogger<DictionaryController> logger,
            IDistributedCache distributedCache)
        {
            _service = service;
            _mapper = mapper;
            _logger = logger;
            _distributedCache = distributedCache;
        }

        /// <summary>
        /// получить катогории событий.
        /// </summary>
        public async override Task<ListCategoryEvent> GetEventCategories(Empty request, ServerCallContext context)
        {
            var result = new ListCategoryEvent() { Status = "Success" };
            const string nowKey = "EventCategories";
            _logger.LogInformation("Запрос категориий событий");
            var serialized = await _distributedCache.GetStringAsync(nowKey);
            if (!String.IsNullOrEmpty(serialized))
            {
                result.Data.AddRange(_mapper
                    .Map<List<CategoryEvent>, List<CategoryEventGRPC>>(JsonSerializer.Deserialize<List<CategoryEvent>>(serialized)));
                return result;
            }
            var res = await _service.GetCategoryEvents();
            _distributedCache.SetStringAsync(
                key: nowKey,
                value: JsonSerializer.Serialize(res),
                options: new DistributedCacheEntryOptions
                {
                    SlidingExpiration = TimeSpan.FromDays(1)
                }).Wait();
            result.Data.AddRange(_mapper.Map<List<CategoryEvent>, List<CategoryEventGRPC>>(res.ToList()));
            return result;
        }

        /// <summary>
        /// получить категории мест.
        /// </summary>
        public async override Task<ListCategoryPlace> GetPlaceCategories(Empty request, ServerCallContext context)
        {
            var result = new ListCategoryPlace() { Status = "Success" };
            const string nowKey = "PlaceCategories";
            _logger.LogInformation("Запрос категориий мест");
            var serialized = await _distributedCache.GetStringAsync(nowKey);
            if (!String.IsNullOrEmpty(serialized))
            {
                result.Data.AddRange(_mapper
                    .Map<List<CategoryPlace>, List<CategoryPlaceGRPC>>(JsonSerializer.Deserialize<List<CategoryPlace>>(serialized)));
                return result;
            }
            var res = await _service.GetCategoryPlaces();
            await _distributedCache.SetStringAsync(
                key: nowKey,
                value: JsonSerializer.Serialize(res),
                options: new DistributedCacheEntryOptions
                {
                    SlidingExpiration = TimeSpan.FromDays(1)
                });
            result.Data.AddRange(_mapper.Map<List<CategoryPlace>, List<CategoryPlaceGRPC>>(res.ToList()));
            return result;
        }

        /// <summary>
        /// получить города.
        /// </summary>
        public async override Task<ListLocation> GetLocations(Empty request, ServerCallContext context)
        {
            var result = new ListLocation() { Status = "Success" };
            const string nowKey = "Locations";
            _logger.LogInformation("Запрос городов");
            var serialized = await _distributedCache.GetStringAsync(nowKey);
            if (!String.IsNullOrEmpty(serialized))
            {
                result.Data.AddRange(_mapper
                    .Map<List<City>, List<LocationGRPC>>(JsonSerializer.Deserialize<List<City>>(serialized)));
                return result;
            }
            var res = await _service.GetCities();
            await _distributedCache.SetStringAsync(
                key: nowKey,
                value: JsonSerializer.Serialize(res),
                options: new DistributedCacheEntryOptions
                {
                    SlidingExpiration = TimeSpan.FromDays(1)
                });
            result.Data.AddRange(_mapper.Map<List<City>, List<LocationGRPC>>(res.ToList()));
            return result;
        }

        /// <summary>
        /// получить теги.
        /// </summary>
        public async override Task<ListTags> GetTags(Empty request, ServerCallContext context)
        {
            var result = new ListTags() { Status = "Success" };
            const string nowKey = "Tag";
            _logger.LogInformation("Запрос тегов");
            var serialized = await _distributedCache.GetStringAsync(nowKey);
            if (!String.IsNullOrEmpty(serialized))
            {
                result.Data.AddRange(_mapper
                    .Map<List<Tag>, List<TagGRPC>>(JsonSerializer.Deserialize<List<Tag>>(serialized)));
                return result;
            }
            var res = await _service.GetTags();
            await _distributedCache.SetStringAsync(
                key: nowKey,
                value: JsonSerializer.Serialize(res),
                options: new DistributedCacheEntryOptions
                {
                    SlidingExpiration = TimeSpan.FromDays(1)
                });
            result.Data.AddRange(_mapper.Map<List<Tag>, List<TagGRPC>>(res.ToList()));
            return result;
        }

        /// <summary>
        /// получить жанры фильмов.
        /// </summary>
        public async override Task<ListMovieGenres> GetMovieGenres(Empty request, ServerCallContext context)
        {
            var result = new ListMovieGenres() { Status = "Success" };
            const string nowKey = "MovieGenres";
            _logger.LogInformation("Запрос жанров фильмов");
            var serialized = await _distributedCache.GetStringAsync(nowKey);
            if (!String.IsNullOrEmpty(serialized))
            {
                result.Data.AddRange(_mapper
                    .Map<List<Genre>, List<MovieGenreGRPC>>(JsonSerializer.Deserialize<List<Genre>>(serialized)));
                return result;
            }
            var res = await _service.GetGenres();
            await _distributedCache.SetStringAsync(
                key: nowKey,
                value: JsonSerializer.Serialize(res),
                options: new DistributedCacheEntryOptions
                {
                    SlidingExpiration = TimeSpan.FromDays(1)
                });
            result.Data.AddRange(_mapper.Map<List<Genre>, List<MovieGenreGRPC>>(res.ToList()));
            return result;
        }
    }
}
