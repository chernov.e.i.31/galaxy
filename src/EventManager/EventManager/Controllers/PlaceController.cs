﻿using AutoMapper;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using SKE.Core.DTO.EventManager;
using SKE.EventManager.Domain;
using SKE.EventManager.Services.Abstractions;
using SKE.EventManager.Services.Contracts;

namespace SKE.EventManager.Controllers
{
    /// <summary>
    /// контроллер мест
    /// </summary>
    [Route("api/[controller]")]
    [ApiController]
    public class PlaceController : ControllerBase
    {
        private IPlaceService _service;
        private IMapper _mapper;
        private readonly ILogger<PlaceController> _logger;

        /// <summary>
        /// cеor контроллер мест
        /// </summary>
        public PlaceController(IPlaceService service, IMapper mapper, ILogger<PlaceController> logger)
        {
            _service = service;
            _mapper = mapper;
            _logger = logger;
        }

        /// <summary>
        /// Получить данные событий согласно переданным фильтрам
        /// </summary>
        /// <returns></returns>
        [HttpGet(Name = "GetPlaces")]
        public async Task<ActionResult<IntegrationResponseDTO<PlaceDTO>>> GetAsync([FromQuery] PlaceFilterDTO placeFilterDTO)
        {
            _logger.LogInformation("Запрос мест");
            var result = await _service.GetPlaces(placeFilterDTO);
            return Ok(
                new IntegrationResponseDTO<PlaceDTO>
                {
                    Status = "Success",
                    Data = _mapper.Map<List<Place>, List<PlaceDTO>>(result.ToList())
                });
        }
    }
}
