﻿using AutoMapper;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using SKE.Core.DTO.EventManager;
using SKE.EventManager.Domain;
using SKE.EventManager.Services.Abstractions;
using SKE.EventManager.Services.Contracts;

namespace SKE.EventManager.Controllers
{
    /// <summary>
    /// контроллер событий
    /// </summary>
    [Route("api/[controller]")]
    [ApiController]
    public class EventController : ControllerBase
    {
        private IEventService _service;
        private IMapper _mapper;
        private readonly ILogger<EventController> _logger;

        /// <summary>
        /// ctor контроллер событий
        /// </summary>
        public EventController(IEventService service, IMapper mapper, ILogger<EventController> logger)
        {
            _service = service;
            _mapper = mapper;
            _logger = logger;
        }

        /// <summary>
        /// Получить данные событий согласно переданным фильтрам
        /// </summary>
        /// <returns></returns>
        [HttpGet(Name="GetEvents")]
        [ProducesResponseType(200, Type = typeof(IntegrationResponseDTO<EventDTO>))]
        [ProducesResponseType(404, Type = typeof(IntegrationResponseDTO<object>))]
        public async Task<ActionResult<IntegrationResponseDTO<EventDTO>>> GetAsync([FromQuery] EventFilterDTO eventFilterDTO)
        {
            _logger.LogInformation("Запрос событий");
            var result = await _service.GetEvents(eventFilterDTO);
            return Ok(
                new IntegrationResponseDTO<EventDTO>
                {
                    Status = "Success",
                    Data = _mapper.Map<List<Event>, List<EventDTO>>(result.ToList())
                });
        }
    }
}
