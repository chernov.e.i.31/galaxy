﻿using AutoMapper;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Caching.Distributed;
using SKE.Core.DTO.EventManager;
using SKE.EventManager.Domain;
using SKE.EventManager.Services.Abstractions;
using System.Text.Json;

namespace SKE.EventManager.Controllers
{
    /// <summary>
    /// Контроллер словарей
    /// </summary>
    [Route("api/[controller]")]
    //[ApiController]
    public class DictionaryController : ControllerBase
    {
        private IDictService _service;
        private IMapper _mapper;
        private readonly ILogger<DictionaryController> _logger;
        private readonly IDistributedCache _distributedCache;

        /// <summary>
        /// ctor контроллер словарей
        /// </summary>
        public DictionaryController(IDictService service,
            IMapper mapper,
            ILogger<DictionaryController> logger,
            IDistributedCache distributedCache)
        {
            _mapper = mapper;
            _service = service;
            _logger = logger;
            _distributedCache = distributedCache;
        }

        /// <summary>
        /// Получить список категорий событий
        /// </summary>
        /// <returns></returns>
        [HttpGet("GetEventCategories")]

        public async Task<ActionResult<IntegrationResponseDTO<CategoryEventDTO>>> GetEventCategoriesAsync()
        {
            const string nowKey = "EventCategories";
            _logger.LogInformation("Запрос категориий событий");
            try {
                var serialized = await _distributedCache.GetStringAsync(nowKey);
                if (!String.IsNullOrEmpty(serialized))
                {
                    return Ok(
                        new IntegrationResponseDTO<CategoryEventDTO>
                        {
                            Status = "Success",
                            Data = _mapper.Map<List<CategoryEvent>, List<CategoryEventDTO>>(
                                JsonSerializer.Deserialize<List<CategoryEvent>>(serialized)
                            )
                        }
                    );
                }
            } catch(Exception ex) {
                _logger.LogInformation("Ошибка обращения к redis", ex.Message);
            }   
            
            var result = await _service.GetCategoryEvents();
            await _distributedCache.SetStringAsync(
                key: nowKey,
                value: JsonSerializer.Serialize(result),
                options: new DistributedCacheEntryOptions
                {
                    SlidingExpiration = TimeSpan.FromDays(1)
                });
            return Ok(
                new IntegrationResponseDTO<CategoryEventDTO>
                {
                    Status = "Success",
                    Data = _mapper.Map<List<CategoryEvent>, List<CategoryEventDTO>>(result.ToList())
                });
        }

        /// <summary>
        /// Получить список категорий мест
        /// </summary>
        /// <returns></returns>
        [HttpGet("GetPlaceCategories")]

        public async Task<ActionResult<IntegrationResponseDTO<CategoryPlaceDTO>>> GetPlaceCategoriesAsync()
        {
            const string nowKey = "PlaceCategories";
            _logger.LogInformation("Запрос категориий мест");
            try
            {
                var serialized = await _distributedCache.GetStringAsync(nowKey);
                if (!String.IsNullOrEmpty(serialized))
                {
                    return Ok(
                        new IntegrationResponseDTO<CategoryPlaceDTO>
                        {
                            Status = "Success",
                            Data = _mapper.Map<List<CategoryPlace>, List<CategoryPlaceDTO>>(
                                JsonSerializer.Deserialize<List<CategoryPlace>>(serialized)
                            )
                        }
                    );
                }
            }
            catch (Exception ex)
            {
                _logger.LogInformation("Ошибка обращения к redis", ex.Message);
            }
            
            var result = await _service.GetCategoryPlaces();
            await _distributedCache.SetStringAsync(
                key: nowKey,
                value: JsonSerializer.Serialize(result),
                options: new DistributedCacheEntryOptions
                {
                    SlidingExpiration = TimeSpan.FromDays(1)
                });
            return Ok(
                new IntegrationResponseDTO<CategoryPlaceDTO>
                {
                    Status = "Success",
                    Data = _mapper.Map<List<CategoryPlace>, List<CategoryPlaceDTO>>(result.ToList())
                });
        }

        /// <summary>
        /// Получить список городов
        /// </summary>
        /// <returns></returns>
        [HttpGet("GetLocations")]
        [ProducesResponseType(200, Type = typeof(IntegrationResponseDTO<CityDTO>))]
        [ProducesResponseType(404, Type = typeof(IntegrationResponseDTO<object>))]
        public async Task<ActionResult<IntegrationResponseDTO<CityDTO>>> GetLocationsAsync()
        {
            const string nowKey = "Locations";
            _logger.LogInformation("Запрос городов");
            try
            {
                var serialized = await _distributedCache.GetStringAsync(nowKey);
                if (!String.IsNullOrEmpty(serialized))
                {
                    return Ok(
                        new IntegrationResponseDTO<CityDTO>
                        {
                            Status = "Success",
                            Data = _mapper.Map<List<City>, List<CityDTO>>(
                                JsonSerializer.Deserialize<List<City>>(serialized)
                            )
                        }
                    );
                }
            }
            catch (Exception ex)
            {
                _logger.LogInformation("Ошибка обращения к redis", ex.Message);
            }
            
            var result = await _service.GetCities();
            await _distributedCache.SetStringAsync(
                key: nowKey,
                value: JsonSerializer.Serialize(result),
                options: new DistributedCacheEntryOptions
                {
                    SlidingExpiration = TimeSpan.FromDays(1)
                });
            return Ok(
                new IntegrationResponseDTO<CityDTO>
                {
                    Status = "Success",
                    Data = _mapper.Map<List<City>, List<CityDTO>>(result.ToList())
                });
        }

        /// <summary>
        /// Получить список тегов 
        /// </summary>
        /// <returns></returns>
        [HttpGet("GetEventTag")]
        [ProducesResponseType(200, Type = typeof(IntegrationResponseDTO<TagDTO>))]
        [ProducesResponseType(404, Type = typeof(IntegrationResponseDTO<object>))]
        public async Task<ActionResult<IntegrationResponseDTO<TagDTO>>> GetTagsAsync()
        {
            const string nowKey = "Tag";
            _logger.LogInformation("Запрос тегов");
            try
            {
                var serialized = await _distributedCache.GetStringAsync(nowKey);
                if (!String.IsNullOrEmpty(serialized))
                {
                    return Ok(
                        new IntegrationResponseDTO<TagDTO>
                        {
                            Status = "Success",
                            Data = _mapper.Map<List<Tag>, List<TagDTO>>(
                                JsonSerializer.Deserialize<List<Tag>>(serialized)
                            )
                        }
                    );
                }
            }
            catch (Exception ex)
            {
                _logger.LogInformation("Ошибка обращения к redis", ex.Message);
            }
            
            var result = await _service.GetTags();
            await _distributedCache.SetStringAsync(
                key: nowKey,
                value: JsonSerializer.Serialize(result),
                options: new DistributedCacheEntryOptions
                {
                    SlidingExpiration = TimeSpan.FromDays(1)
                });
            return Ok(
                new IntegrationResponseDTO<TagDTO>
                {
                    Status = "Success",
                    Data = _mapper.Map<List<Tag>, List<TagDTO>>(result.ToList())
                });
        }

        /// <summary>
        /// Получить список жанры фильмов
        /// </summary>
        /// <returns></returns>
        [HttpGet("GetMovieGenres")]
        [ProducesResponseType(200, Type = typeof(IntegrationResponseDTO<GenreDTO>))]
        [ProducesResponseType(404, Type = typeof(IntegrationResponseDTO<object>))]
        public async Task<ActionResult<IntegrationResponseDTO<GenreDTO>>> GetMovieGenresAsync()
        {
            const string nowKey = "MovieGenres";
            _logger.LogInformation("Запрос жанров фильмов");
            try
            {
                var serialized = await _distributedCache.GetStringAsync(nowKey);
                if (!String.IsNullOrEmpty(serialized))
                {
                    return Ok(
                        new IntegrationResponseDTO<GenreDTO>
                        {
                            Status = "Success",
                            Data = _mapper.Map<List<Genre>, List<GenreDTO>>(
                                JsonSerializer.Deserialize<List<Genre>>(serialized)
                            )
                        }
                    );
                }
            }
            catch (Exception ex)
            {
                _logger.LogInformation("Ошибка обращения к redis", ex.Message);
            }
            
            var result = await _service.GetGenres();
            await _distributedCache.SetStringAsync(
                key: nowKey,
                value: JsonSerializer.Serialize(result),
                options: new DistributedCacheEntryOptions
                {
                    SlidingExpiration = TimeSpan.FromDays(1)
                });
            return Ok(
                new IntegrationResponseDTO<GenreDTO>
                {
                    Status = "Success",
                    Data = _mapper.Map<List<Genre>, List<GenreDTO>>(result.ToList())
                });
        }
    }
}
