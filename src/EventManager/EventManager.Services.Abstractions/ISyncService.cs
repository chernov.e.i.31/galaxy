﻿namespace SKE.EventManager.Services.Abstractions
{
    /// <summary>
    /// Cервис синхронизации данных с внешними источникамии
    /// </summary>
    public interface ISyncService
    {
        /// <summary>
        /// синхронизировать данные
        /// </summary>
        Task SyncData();
    }
}
