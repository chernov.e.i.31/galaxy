﻿using AutoMapper;
using EMD = SKE.EventManager.Domain;
using KGD = SKE.KudaGo.Domain.Model;

namespace SKE.EventManager.Services.Implementations.Mapper
{
    /// <summary>
    /// Профиль автомаппера для сущности место.
    /// </summary>
    public class PlaceMappings : Profile
    {
        public PlaceMappings()
        {
            CreateMap<KGD.Place, EMD.Place>()
                .ForMember(d => d.ExternalId, map => map.MapFrom(s => s.Id))
                .ForMember(d => d.Id, map => map.Ignore())
                .ForMember(d => d.Location, map => map.Ignore())
                .ForMember(d => d.Categories, map => map.Ignore())
                .ForMember(d => d.Tags, map => map.Ignore());
        }
    }
}
