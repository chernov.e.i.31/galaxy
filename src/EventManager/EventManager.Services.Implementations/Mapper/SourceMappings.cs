﻿using AutoMapper;
using EF = SKE.EventManager.Domain;
using KG = SKE.KudaGo.Domain.Model;

namespace SKE.EventManager.Services.Implementations.Mapper
{
    /// <summary>
    /// Профиль автомаппера для сущности источника.
    /// </summary>
    public class SourceMappings : Profile
    {
        public SourceMappings()
        {
            CreateMap<KG.Source, EF.Source>();
        }
    }
}
