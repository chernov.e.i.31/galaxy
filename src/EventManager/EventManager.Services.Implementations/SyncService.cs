﻿using AutoMapper;
using SKE.EventManager.Services.Abstractions;
using SKE.EventManager.Services.Contracts;
using EM = SKE.EventManager.Services.Repositories.Abstractions;
using EMD = SKE.EventManager.Domain;
using KG = SKE.KudaGo.Services.Repositories.Abstractions;
using KGD = SKE.KudaGo.Domain.Model;

namespace SKE.EventManager.Services.Implementations
{
    public class SyncService : ISyncService
    {
        private readonly IMapper _mapper;
        private readonly KG.ICategoryEventRepository _categoryEventRepository;
        private readonly KG.ICategoryPlaceRepository _categoryPlaceRepository;
        private readonly KG.ICityRepository _cityRepository;
        private readonly KG.IEventRepository _eventRepository;
        private readonly KG.IMovieRepository _movieRepository;
        private readonly KG.IMovieShowingsRepository _movieShowingsRepository;
        private readonly KG.IPlaceRepository _placeRepository;
        private readonly EM.ICategoryEventRepository _categoryEventBD;
        private readonly EM.ICategoryPlaceRepository _categoryPlaceBD;
        private readonly EM.ICityRepository _cityBD;
        private readonly EM.IEventRepository _eventBD;
        private readonly EM.IGenreRepository _genreBD;
        private readonly EM.IMovieRepository _movieBD;
        private readonly EM.IMovieShowingRepository _movieShowingBD;
        private readonly EM.IPlaceRepository _placeBD;
        private readonly EM.ITagRepository _tagBD;

        public SyncService(
            KG.ICategoryEventRepository categoryEventRepository,
            KG.ICategoryPlaceRepository categoryPlaceRepository,
            KG.ICityRepository cityRepository,
            KG.IEventRepository eventRepository,
            KG.IMovieRepository movieRepository,
            KG.IMovieShowingsRepository movieShowingsRepository,
            KG.IPlaceRepository placeRepository,
            EM.ICategoryEventRepository categoryEventBD,
            EM.ICategoryPlaceRepository categoryPlaceBD,
            EM.ICityRepository cityBD,
            EM.IEventRepository eventBD,
            EM.IGenreRepository genreBD,
            EM.IMovieRepository movieBD,
            EM.IMovieShowingRepository movieShowingBD,
            EM.IPlaceRepository placeBD,
            EM.ITagRepository tagBD,
            IMapper mapper)
        {
            _categoryEventRepository = categoryEventRepository;
            _categoryPlaceRepository = categoryPlaceRepository;
            _cityRepository = cityRepository;
            _eventRepository = eventRepository;
            _movieRepository = movieRepository;
            _movieShowingsRepository = movieShowingsRepository;
            _placeRepository = placeRepository;

            _categoryEventBD = categoryEventBD;
            _categoryPlaceBD = categoryPlaceBD;
            _cityBD = cityBD;
            _eventBD = eventBD;
            _genreBD = genreBD;
            _movieBD = movieBD;
            _movieShowingBD = movieShowingBD;
            _placeBD = placeBD;
            _tagBD = tagBD;

            _mapper = mapper;
        }

        public async Task SyncData()
        {
            await SyncCategoryEventAsync();
            await SyncCategoryPlaceAsync();
            await SyncLocationAsync();
            await SyncPlaceAsync();
            await SyncEventAsync();
            //await SyncMoviesAsync();
            //await SyncMovieShowAsync();
        }

        private async Task SyncCategoryEventAsync()
        {
            var elements = await _categoryEventRepository.GetAllAsync();
            var elementsInBD = await _categoryEventBD.GetFitleredAsync();

            var elementsNotExist = elements.Where(x => !elementsInBD.Select(t => t.ExternalId).Contains(x.Id));

            foreach (var r in elementsNotExist.Select(x => _mapper.Map<KGD.CategoryEvent, EMD.CategoryEvent>(x)).ToList())
            {
                await _categoryEventBD.AddAsync(r);
                await _categoryEventBD.SaveChangesAsync();
            }
            elements = null;
            elementsInBD = null;
            elementsNotExist = null;
        }

        private async Task SyncCategoryPlaceAsync()
        {
            var elements = await _categoryPlaceRepository.GetAllAsync();
            var elementsInBD = await _categoryPlaceBD.GetFitleredAsync();

            var elementsNotExist = elements.Where(x => !elementsInBD.Select(t => t.ExternalId).Contains(x.Id));

            foreach (var r in elementsNotExist.Select(x => _mapper.Map<KGD.CategoryPlace, EMD.CategoryPlace>(x)).ToList())
            {
                await _categoryPlaceBD.AddAsync(r);
                await _categoryPlaceBD.SaveChangesAsync();
            }
            elements = null;
            elementsInBD = null;
            elementsNotExist = null;
        }

        private async Task SyncLocationAsync()
        {
            var elements = await _cityRepository.GetAllAsync();
            var elementsInBD = await _cityBD.GetFitleredAsync();

            var elementsNotExist = elements.Where(x => !elementsInBD.Select(t => t.Slug).Contains(x.Slug));

            foreach (var r in elementsNotExist.Select(x => _mapper.Map<KGD.City, EMD.City>(x)).ToList())
            {
                await _cityBD.AddAsync(r);
                await _cityBD.SaveChangesAsync();
            }
            elements = null;
            elementsInBD = null;
            elementsNotExist = null;
        }

        private async Task SyncPlaceAsync()
        {
            var elementsInBD = (await _placeBD.GetAllAsync(cancellationToken: new CancellationToken())).Select(x => x.ExternalId).ToList();
            var categoryPlaces = await _categoryPlaceBD.GetFitleredAsync();
            var cities = await _cityBD.GetFitleredAsync();
            await foreach (var elements in _placeRepository.GetAllAsync())
            {
                var tags = elements.Select(x => x.Tags).ToList();
                var tagList = new List<string>();
                foreach (var t in tags)
                {
                    tagList.AddRange(t);
                }
                var tagPlaces = await SyncTags(tagList.Distinct());
                var elementsNotExist = elements.Where(x => !elementsInBD.Contains(x.Id)).ToList();
                foreach (var r in elementsNotExist)
                {
                    var location = cities.Where(x => x.Slug == r.Location).FirstOrDefault();
                    if (location is not null)
                    {
                        var elem = _mapper.Map<KGD.Place, EMD.Place>(r);
                        elem.LocationId = location.Id;
                        categoryPlaces
                            .Where(x => r.Categories.Contains(x.Slug))
                            .ToList()
                            .ForEach(o => elem.Categories.Add(o));
                        tagPlaces
                            .Where(x => r.Tags.Contains(x.Name))
                            .ToList()
                            .ForEach(o => elem.Tags.Add(o));

                        await _placeBD.AddAsync(elem);
                        await _placeBD.SaveChangesAsync();
                        elementsInBD.Add(elem.ExternalId);
                    }
                }
                elementsNotExist = null;
                tags = null;
                tagList = null;
                tagPlaces = null;
            }
            elementsInBD = null;
            categoryPlaces = null;
            cities = null;
        }

        private async Task SyncEventAsync()
        {
            var elementsInBD = (await _eventBD.GetAllAsync(cancellationToken: new CancellationToken())).Select(x => x.ExternalId).ToList();
            var categoryEvent = await _categoryEventBD.GetFitleredAsync();
            var cities = await _cityBD.GetFitleredAsync();
            var places = (await _placeBD.GetAllAsync(cancellationToken: new CancellationToken())).Select(x => new { x.Id, x.ExternalId }).ToList();

            await foreach (var elements in _eventRepository.GetAllAsync())
            {
                var tags = elements.Select(x => x.Tags).ToList();
                var tagList = new List<string>();
                foreach (var t in tags)
                {
                    tagList.AddRange(t);
                }
                var tagEvents = await SyncTags(tagList.Distinct());
                var elementsNotExist = elements.Where(x => !elementsInBD.Contains(x.Id)).ToList();
                foreach (var r in elementsNotExist)
                {
                    var location = cities.Where(x => x.Slug == r.Location.Slug).FirstOrDefault();
                    if (location is not null)
                    {
                        var elem = _mapper.Map<KGD.Event, EMD.Event>(r);
                        elem.LocationId = location.Id;
                        var placeId = places.Where(x => x.ExternalId == r.Place?.Id).Select(x => x.Id).FirstOrDefault();
                        elem.PlaceId = placeId != default ? placeId : null;
                        categoryEvent
                            .Where(x => r.Categories.Contains(x.Slug))
                            .ToList()
                            .ForEach(o => elem.Categories.Add(o));
                        tagEvents
                            .Where(x => r.Tags.Contains(x.Name))
                            .ToList()
                            .ForEach(o => elem.Tags.Add(o));

                        await _eventBD.AddAsync(elem);
                        await _eventBD.SaveChangesAsync();
                        elementsInBD.Add(elem.ExternalId);
                    }
                }
                elementsNotExist = null;
                tags = null;
                tagList = null;
                tagEvents = null;
            }
            elementsInBD = null;
            categoryEvent = null;
            cities = null;
            places = null;
        }

        private async Task SyncMoviesAsync()
        {
            var elementsInBD = await _movieBD.GetFitleredAsync(new MovieFilterDTO());

            await foreach (var elements in _movieRepository.GetAllAsync())
            {
                var genres = elements.Select(x => x.Genres).ToList();
                var genreList = new List<KGD.Genre>();
                foreach (var t in genres)
                {
                    genreList.AddRange(t);
                }
                var genresMovie = await SyncGenres(genreList.DistinctBy(x => x.Id));
                var elementsNotExist = elements.Where(x => !elementsInBD.Select(t => t.ExternalId).Contains(x.Id));
                foreach (var element in elementsNotExist.ToList())
                {
                    var elem = _mapper.Map<KGD.Movie, EMD.Movie>(element);

                    genresMovie
                        .Where(x => element.Genres.Select(x => x.Id).Contains((int)x.ExternalId))
                        .ToList()
                        .ForEach(o => elem.Genres.Add(o));
                    await _movieBD.AddAsync(elem);
                    await _movieBD.SaveChangesAsync();
                }
                elementsNotExist = null;
                genres = null;
                genreList = null;
                genresMovie = null;
            }
            elementsInBD = null;
        }

        private async Task SyncMovieShowAsync()
        {
            var movies = (await _movieBD.GetFitleredAsync(new MovieFilterDTO()))
                .Select(x => new { id = x.Id, ExternalId = x.ExternalId })
                .ToList();

            var places = (await _placeBD.GetFitleredAsync(new PlaceFilterDTO()))
                .Select(x => new { id = x.Id, ExternalId = x.ExternalId })
                .ToList();

            foreach (var movie in movies)
            {
                if (movie.ExternalId.HasValue)
                {
                    var elementsInBD = await _movieShowingBD.GetFitleredAsync(new MovieShowingFilterDTO { movieExternalId = movie.ExternalId });
                    await foreach (var elements in _movieShowingsRepository.GetAllAsync(movie.ExternalId.Value))
                    {
                        var elementsNotExist = elements.Where(x => !elementsInBD.Select(t => t.ExternalId).Contains(x.Id));
                        foreach (var element in elementsNotExist.ToList())
                        {
                            if (element.Place is not null)
                            {
                                var place = places.Where(x => x.ExternalId == element.Place.Id).FirstOrDefault();
                                if (place != null)
                                {
                                    var show = _mapper.Map<KGD.MovieShowings, EMD.MovieShowings>(element);
                                    show.MovieId = movie.id;
                                    show.PlaceId = place.id;
                                    await _movieShowingBD.AddAsync(show);
                                    await _movieShowingBD.SaveChangesAsync();
                                }
                            }
                        }
                        elementsInBD = null;
                        elementsNotExist = null;
                    }
                }
            }
            movies = null;
            places = null;
        }

        private async Task<IEnumerable<EMD.Tag>> SyncTags(IEnumerable<string> tags)
        {
            var tagsBD = await _tagBD.GetFitleredAsync();
            var notExistTags = tags.Where(x => !tagsBD.Select(t => t.Name).Contains(x)).ToList();
            foreach (var tag in notExistTags)
            {
                await _tagBD.AddAsync(new EMD.Tag { Name = tag });
                await _tagBD.SaveChangesAsync();
            }
            tagsBD = null;
            notExistTags = null;
            return await _tagBD.GetFitleredAsync();
        }

        private async Task<IEnumerable<EMD.Genre>> SyncGenres(IEnumerable<KGD.Genre> genres)
        {
            var genresBD = await _genreBD.GetFitleredAsync();
            var notExistGenres = genres.Where(x => !genresBD.Select(t => t.ExternalId).Contains(x.Id)).ToList();
            foreach (var genre in notExistGenres)
            {
                await _genreBD.AddAsync(_mapper.Map<KGD.Genre, EMD.Genre>(genre));
                await _genreBD.SaveChangesAsync();
            }
            genresBD = null;
            notExistGenres = null;
            return await _genreBD.GetFitleredAsync();
        }
    }
}