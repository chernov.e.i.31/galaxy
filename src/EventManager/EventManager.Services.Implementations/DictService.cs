﻿using SKE.EventManager.Domain;
using SKE.EventManager.Services.Abstractions;
using SKE.EventManager.Services.Repositories.Abstractions;

namespace SKE.EventManager.Services.Implementations
{
    public class DictService : IDictService
    {
        ICategoryEventRepository _categoryEvent;
        ICategoryPlaceRepository _categoryPlace;
        ICityRepository _city;
        IGenreRepository _genre;
        ITagRepository _tag;
        public DictService(
            ICategoryEventRepository categoryEvent,
            ICategoryPlaceRepository categoryPlace,
            ICityRepository city,
            IGenreRepository genre,
            ITagRepository tag)
        {
            _categoryEvent = categoryEvent;
            _categoryPlace = categoryPlace;
            _city = city;
            _genre = genre;
            _tag = tag;
        }
        public async Task<IEnumerable<CategoryEvent>> GetCategoryEvents()
        {
            return await _categoryEvent.GetFitleredAsync();
        }

        public async Task<IEnumerable<CategoryPlace>> GetCategoryPlaces()
        {
            return await _categoryPlace.GetFitleredAsync();
        }

        public async Task<IEnumerable<City>> GetCities()
        {
            return await _city.GetFitleredAsync();
        }

        public async Task<IEnumerable<Genre>> GetGenres()
        {
            return await _genre.GetFitleredAsync();
        }

        public async Task<IEnumerable<Tag>> GetTags()
        {
            return await _tag.GetFitleredAsync();
        }
    }
}
