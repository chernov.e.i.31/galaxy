﻿
using SKE.Core.Repositories.Abstractions;
using SKE.EventManager.Domain;
using SKE.EventManager.Services.Contracts;

namespace SKE.EventManager.Services.Repositories.Abstractions
{
    /// <summary>
    /// Репозиторий работы с местами
    /// </summary>
    public interface IPlaceRepository : IRepository<Place, Guid>
    {
        /// <summary>
        /// Получить постраничный список.
        /// </summary>
        /// <param name="filterDTO"> ДТО фильтра. </param>
        /// <returns> Список мест. </returns>
        Task<List<Place>> GetFitleredAsync(PlaceFilterDTO filterDTO);
    }
}
