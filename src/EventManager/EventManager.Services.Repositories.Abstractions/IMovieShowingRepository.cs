﻿
using SKE.Core.Repositories.Abstractions;
using SKE.EventManager.Domain;
using SKE.EventManager.Services.Contracts;

namespace SKE.EventManager.Services.Repositories.Abstractions
{
    /// <summary>
    /// Репозиторий работы с показами фильмов
    /// </summary>
    public interface IMovieShowingRepository : IRepository<MovieShowings, Guid>
    {
        /// <summary>
        /// Получить список.
        /// </summary>
        /// <param name="filterDTO"> ДТО фильтра. </param>
        /// <returns> Список показов фильма. </returns>
        Task<List<MovieShowings>> GetFitleredAsync(MovieShowingFilterDTO filterDTO);
    }
}
