﻿using SKE.KudaGo.Domain.Model;
using SKE.KudaGo.Services.Repositories.Abstractions;
using System.Collections;
using System.Collections.Generic;

namespace SKE.KudaGo.Repositories.Implementations
{
    /// <summary>
    /// Репозиторий работы с событиями
    /// </summary>
    public class EventRepository : Repository<KudaGoResponse<Event>>, IEventRepository
    {
        bool hasNextPage;
        int pageNumber = 1;

        private readonly string _urlPath = "events/?page_size=100" +
            "&fields=id,publication_date,dates,title,short_title,slug,place,description,body_text,location,categories,tagline,age_restriction,price," +
            "is_free,images,favorites_count,comments_count,site_url,tags,participants" +
            "&expand=images,place,location,dates,participants" +
            "&order_by=id" +
            "&actual_since=" + DateTimeOffset.Now.ToUnixTimeSeconds();

        /// <summary>
        /// Получить список событий
        /// </summary>
        /// <returns>список событий</returns>
        public async IAsyncEnumerable<IEnumerable<Event>> GetAllAsync()
        {
            do
            {
                var requestString = _urlPath + "&page=" + pageNumber;
                var response = await GetAllQueryAsync(requestString);
                yield return response.Results;
                hasNextPage = response.Next is null ? false : true;
                pageNumber++;
            }
            while (hasNextPage);
        }
    }
}
