﻿using SKE.KudaGo.Domain.Model;
using SKE.KudaGo.Services.Repositories.Abstractions;

namespace SKE.KudaGo.Repositories.Implementations
{
    /// <summary>
    /// Репозиторий работы с местами
    /// </summary>
    public class PlaceRepository : Repository<KudaGoResponse<Place>>, IPlaceRepository
    {
        bool hasNextPage;
        int pageNumber = 1;

        private readonly string _urlPath = "places/?page_size=100" +
            "&fields=id,title,short_title,slug,address,location,timetable,phone,is_stub,images,description," +
            "body_text,site_url,foreign_url,coords,subway,favorites_count,comments_count,is_closed,categories,tags" +
            "&expand=images" +
            "&order_by=id";

        /// <summary>
        /// Получить список мест
        /// </summary>
        /// <returns>список мест</returns>
        public async IAsyncEnumerable<IEnumerable<Place>> GetAllAsync()
        {
            do
            {
                var requestString = _urlPath + "&page=" + pageNumber;
                var response = await GetAllQueryAsync(requestString);
                yield return response.Results;
                hasNextPage = response.Next is null ? false : true;
                pageNumber++;
            }
            while (hasNextPage);
        }
    }
}
