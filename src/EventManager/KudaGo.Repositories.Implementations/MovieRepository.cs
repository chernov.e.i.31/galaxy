﻿using SKE.KudaGo.Domain.Model;
using SKE.KudaGo.Services.Repositories.Abstractions;

namespace SKE.KudaGo.Repositories.Implementations
{
    /// <summary>
    /// Репозиторий работы с фильмами
    /// </summary>
    public class MovieRepository : Repository<KudaGoResponse<Movie>>, IMovieRepository
    {
        bool hasNextPage;
        int pageNumber = 1;

        private readonly string _urlPath = "movies/?page_size=100" +
            "&fields=id,site_url,publication_date,slug,title,description,body_text,is_editors_choice,favorites_count," +
            "genres,comments_count,original_title,locale,country,year,language,running_time,budget_currency,budget," +
            "mpaa_rating,age_restriction,stars,director,writer,awards,trailer,images,poster,url,imdb_url,imdb_rating" +
            "&expand=images,poster" +
            "&order_by=id" +
            "&actual_since=" + DateTimeOffset.Now.ToUnixTimeSeconds();

        /// <summary>
        /// Получить список фильмов
        /// </summary>
        /// <returns>список фильмов</returns>
        /// <returns>список фильмов</returns>
        public async IAsyncEnumerable<IEnumerable<Movie>> GetAllAsync()
        {
            do
            {
                var requestString = _urlPath + "&page=" + pageNumber;
                var response = await GetAllQueryAsync(requestString);
                yield return response.Results;
                hasNextPage = response.Next is null ? false : true;
                pageNumber++;
            }
            while (hasNextPage);
        }
    }
}
