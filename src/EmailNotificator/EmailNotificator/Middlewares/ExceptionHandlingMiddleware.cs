﻿using System.Net;

namespace SKE.EmailNotificator.Middlewares
{
    /// <summary>
    /// Middleware обработки исключений
    /// </summary>
    public class ExceptionHandlingMiddleware
    {
        private readonly RequestDelegate _next;
        private readonly ILogger<ExceptionHandlingMiddleware> _logger;
        private readonly IWebHostEnvironment _env;

        /// <summary>
        /// ctor обработки исключений
        /// </summary>
        public ExceptionHandlingMiddleware(
            RequestDelegate next,
            ILogger<ExceptionHandlingMiddleware> logger,
            IWebHostEnvironment env)
        {
            _logger = logger;
            _next = next;
            _env = env;
        }

        /// <summary>
        /// выполнение команды
        /// </summary>
        public async Task InvokeAsync(HttpContext httpContext)
        {
            try
            {
                await _next(httpContext);
            }
            catch (Exception ex)
            {
                await HandleExceptionAsync(httpContext, ex);
            }
        }

        private async Task HandleExceptionAsync(
            HttpContext httpContext,
            Exception ex)
        {
            var exceptionId = Guid.NewGuid();
            _logger.LogError(
                ex,
                "ExceptionId: {0}; ErrorMessage:{1}; Env:{2}",
                exceptionId,
                ex.Message,
                _env.EnvironmentName
            );

            HttpResponse response = httpContext.Response;

            response.ContentType = "application/json";
            response.StatusCode = (int)HttpStatusCode.InternalServerError;
            await response.WriteAsJsonAsync(new
            {
                Error = _env.IsProduction() ? exceptionId.ToString() : ex.Message
            });
        }
    }
}
