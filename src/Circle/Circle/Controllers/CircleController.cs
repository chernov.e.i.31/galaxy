﻿using SKE.Circle.Services.Contracts;
using AutoMapper;
using Microsoft.AspNetCore.Mvc;
using SKE.Circle.Domain;
using SKE.Circle.Services.Repositories.Abstractions;
using Microsoft.AspNetCore.Authorization;

namespace SKE.Circle.Controllers
{
    /// <summary>
    /// контроллер кругов
    /// </summary>
    [Authorize()]
    [ApiController]
    [Route("api/v1/[controller]")]
    public class CircleController
        : ControllerBase
    {
        private readonly IRepository<CircleInfo> _circleRepository;
        private IMapper _mapper;

        /// <summary>
        /// ctor кругов.
        /// </summary>
        public CircleController(IRepository<CircleInfo> circleRepository, IMapper mapper)
        {
            _circleRepository = circleRepository;
            _mapper = mapper;
        }

        /// <summary>
        /// Получить список кругов по пользователю
        /// </summary>
        /// <param name="user">email пользователя, например <example>user@test.com</example></param>
        /// <returns></returns>
        [HttpGet]
        public async Task<List<CircleInfoDTO>> GetCirckesByUserAsync(string user)
        {
            var circles = await _circleRepository
                .GetWhere(x => x.Users.Contains(user));
            return _mapper.Map<List<CircleInfo>, List<CircleInfoDTO>>(circles.ToList());
        }

        /// <summary>
        /// создать круг
        /// </summary>
        /// <param name="user">email пользователя, например <example>user@test.com</example></param>
        /// <param name="name">наименование круга<example>simple text</example></param>
        /// <returns></returns>
        [HttpPost]
        public async Task<IActionResult> AddCircleAsync([FromBody] CircleModel circleModel)
        {
            var circleWithUserExist = await _circleRepository.GetWhere(x => x.Name.Equals(circleModel.Name) && x.Users.Contains(circleModel.User));
            if (circleWithUserExist.FirstOrDefault() is not null)
            {
                return BadRequest("Круг с таким наименованием уже существует");
            }
            var сircleModel = new CircleInfo()
            {
                Name = circleModel.Name,
                Users = new List<string>() { circleModel.User }
            };
            var id = await _circleRepository.AddAsync(сircleModel);
            return Ok(id);
        }

        /// <summary>
        /// удалить круг
        /// </summary>
        /// <param name="circeId">Id круга, например <example>451533d5-d8d5-4a11-9c7b-eb9f14e1a32f</example></param>
        /// <returns></returns>
        [HttpDelete]
        public async Task<IActionResult> RemoveCircleAsync(Guid circeId)
        {
            var circle = await _circleRepository.GetByIdAsync(circeId);

            if (circle == null)
                return NotFound();

            await _circleRepository.DeleteAsync(circle);

            return Ok();
        }
    }
}