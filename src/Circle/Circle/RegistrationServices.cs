﻿using AutoMapper;
using Microsoft.AspNetCore.Authentication.JwtBearer;
using Microsoft.IdentityModel.Tokens;
using Microsoft.OpenApi.Models;
using Serilog;
using Serilog.Sinks.Elasticsearch;
using SKE.Circle.DataBase;
using SKE.Circle.Mapping;
using SKE.Circle.Repositories.Implementations;
using SKE.Circle.Services.Repositories.Abstractions;
using System.Reflection;
using System.Text;

namespace SKE.Circle
{
    /// <summary>
    /// расширение регистрации.
    /// </summary>
    public static class RegistrationServices
    {
        /// <summary>
        /// Добавление сервисов.
        /// </summary>
        public static IServiceCollection AddServices(this IServiceCollection services, IConfiguration configuration)
        {
            services.InstallDatabase(configuration);
            services.InstallAutomaper();
            services.InstallRepositories();
            services.InstallSwagger();
            return services;
        }

        /// <summary>
        /// Добавление логирования.
        /// </summary>
        public static IHostBuilder AddHosts(this IHostBuilder services, IConfiguration config)
        {
            services.UseSerilog((context, services, configuration) =>
                configuration
                .ReadFrom.Configuration(context.Configuration)
                .Enrich.FromLogContext()
                .WriteTo.Elasticsearch(new ElasticsearchSinkOptions(new Uri(config.GetConnectionString("Elastic").ToString()))
                {
                    FailureCallback = e =>
                    {
                        Console.WriteLine("Unable to submit event " + e.Exception);
                    },
                    TypeName = null,
                    IndexFormat = "Circle-Log",
                    AutoRegisterTemplate = true,
                    EmitEventFailure = EmitEventFailureHandling.ThrowException | EmitEventFailureHandling.RaiseCallback | EmitEventFailureHandling.WriteToSelfLog
                })
            );
            return services;
        }

        private static IServiceCollection InstallDatabase(this IServiceCollection services, IConfiguration configuration)
        {
            services.ConfigureContext(configuration.GetConnectionString("DefaultConnection"));
            return services;
        }

        private static IServiceCollection InstallSwagger(this IServiceCollection services)
        {
            services.AddSwaggerGen(c =>
            {
                c.SwaggerDoc("v1", new OpenApiInfo
                {
                    Title = "API Documentation",
                    Version = "v1"
                });
                var entryAssembly = Assembly.GetEntryAssembly();
                if (entryAssembly != null)
                {
                    var xmlFile = Path.ChangeExtension(entryAssembly.Location, ".xml");
                    c.IncludeXmlComments(xmlFile);
                }

                c.AddSecurityDefinition("Bearer", new OpenApiSecurityScheme()
                {
                    Name = "Authorization",
                    Type = SecuritySchemeType.ApiKey,
                    Scheme = "Bearer",
                    BearerFormat = "JWT",
                    In = ParameterLocation.Header,
                    Description = "JWT Authorization header using the Bearer scheme. \r\n\r\n Enter 'Bearer' [space] and then your token in the text input below.\r\n\r\nExample: \"Bearer 12345abcdef\"",
                });
                c.AddSecurityRequirement(new OpenApiSecurityRequirement {
                {
                    new OpenApiSecurityScheme
                    {
                        Reference = new OpenApiReference
                        {
                            Type = ReferenceType.SecurityScheme,
                            Id = "Bearer"
                        }
                    },
                    new string[] {}
                }});
            });
            return services;
        }

        private static IServiceCollection InstallRepositories(this IServiceCollection services)
        {
            services.AddScoped(typeof(IRepository<>), typeof(DBRepository<>));
            return services;
        }

        private static IServiceCollection InstallAutomaper(this IServiceCollection services)
        {
            var configuration = new MapperConfiguration(cfg =>
            {
                cfg.AddProfile<CircleDTOMappingsProfile>();
                cfg.AddProfile<EventDTOMappingsProfile>();
            });
            services.AddSingleton<IMapper>(new Mapper(configuration));
            return services;
        }
    }
}
