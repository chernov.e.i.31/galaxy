﻿using SKE.Circle.Services.Contracts;
using AutoMapper;
using SKE.Circle.Domain;

namespace SKE.Circle.Mapping
{
    /// <summary>
    /// Профиль автомаппера для информации о мероприятии.
    /// </summary>
    public class EventDTOMappingsProfile : Profile
    {
        /// <summary>
        /// ctor Профиль автомаппера для информации о мероприятии
        /// </summary>
        public EventDTOMappingsProfile()
        {
            CreateMap<EventInfo, EventInfoDTO>();
            CreateMap<EventInfoDTO, EventInfo>();
        }
    }
}
