﻿using MongoDB.Driver;
using SKE.Circle.Services.Repositories.Abstractions;
using SKE.CoreLib.Core;
using System.Linq.Expressions;

namespace SKE.Circle.Repositories.Implementations
{
    public class DBRepository<T>
        : IRepository<T>
        where T : BaseEntity
    {
        private IMongoCollection<T> _collection;
        public DBRepository(IMongoCollection<T> collection)
        {
            _collection = collection;
        }
        public async Task<Guid> AddAsync(T entity)
        {
            entity.Id = Guid.NewGuid();
            await _collection.InsertOneAsync(entity);
            return entity.Id;
        }

        public async Task DeleteAsync(T entity)
        {
            var filter = Builders<T>.Filter.Eq(x => x.Id, entity.Id);
            await _collection.DeleteOneAsync(filter);
        }

        public async Task<IEnumerable<T>> GetAllAsync()
        {
            return await _collection.Find(Builders<T>.Filter.Empty).ToListAsync();
        }

        public async Task<T> GetByIdAsync(Guid id)
        {
            return await _collection.Find(x => x.Id == id).FirstOrDefaultAsync();
        }

        public async Task<T> GetFirstWhere(Expression<Func<T, bool>> predicate)
        {
            return await _collection.Find(predicate).FirstOrDefaultAsync();
        }

        public async Task<IEnumerable<T>> GetRangeByIdsAsync(List<Guid> ids)
        {
            return await _collection.Find(x => ids.Contains(x.Id)).ToListAsync();
        }

        public async Task<IEnumerable<T>> GetWhere(Expression<Func<T, bool>> predicate)
        {
            return await _collection.Find(predicate).ToListAsync();
        }

        public async Task UpdateAsync(T entity)
        {
            await _collection.ReplaceOneAsync(x => x.Id == entity.Id, entity);
        }
    }
}