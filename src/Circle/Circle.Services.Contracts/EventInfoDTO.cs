﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SKE.Circle.Services.Contracts
{
    public class EventInfoDTO
    {
        public Guid Id { get; set; }

        /// <summary>
        /// Дата добавления в круг
        /// </summary>
        public DateTime DateAdd { get; set; }

        /// <summary>
        /// ссылка на событие
        /// </summary>
        public Guid EventId { get; set; }
    }
}
